package name.maxaller.myparadise

import com.badlogic.gdx.math.Vector2
import org.apache.commons.math3.analysis.polynomials.PolynomialFunctionLagrangeForm

class LagrangePolynomial(points: List<Vector2>) {
    private val instance = PolynomialFunctionLagrangeForm(points.extractX(), points.extractY())

    private fun List<Vector2>.extractX() = DoubleArray(size) { i -> this[i].x.toDouble() }
    private fun List<Vector2>.extractY() = DoubleArray(size) { i -> this[i].y.toDouble() }

    fun valueAt(x: Float) = instance.value(x.toDouble()).toFloat()

//    fun valueAt(x: Float) = points.mapIndexed { i, point ->
//        val numerator = points.foldIndexed(1F) { i2, total, otherPoint ->
//            if (i == i2) total
//            else total * (x - otherPoint.x)
//        }
//        val denominator = points.foldIndexed(1F) { i2, total, otherPoint ->
//            if (i == i2) total
//            else total * (point.x - otherPoint.x)
//        }
//        val coefficient = point.y
//        coefficient * (numerator / denominator)
//    }.sum()
}