package name.maxaller.myparadise

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.EntityListener
import com.badlogic.ashley.core.Family
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.ScreenAdapter
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.physics.box2d.BodyDef
import com.badlogic.gdx.physics.box2d.World
import com.google.inject.Inject
import com.google.inject.Injector

class GameScreen @Inject constructor(private val engine: Engine,
                                     gameInitializer: GameInitializer,
                                     private val gameSaveManager: GameSaveManager,
                                     injector: Injector) : ScreenAdapter() {
    init {
        setupEngine(injector)
        gameInitializer.initialize()
    }

    override fun render(delta: Float) {
        Gdx.gl.glClearColor(0.3f, 0.3f, 0.3f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT or GL20.GL_DEPTH_BUFFER_BIT)

        engine.update(Gdx.graphics.deltaTime)
        gameSaveManager.saveIfRequested()
        gameSaveManager.loadIfRequested()
    }

    private fun setupEngine(injector: Injector) {
        sequenceOf(
                CreatureAiBounceSystem::class.java,
                PlayerPickupSystem::class.java,
                DaylightSystem::class.java,
                TerrainTickerSystem::class.java,
                PeriodicLightingRelighter::class.java,

                PlayerControllerSystem::class.java,
                LinkedInteractionSystem::class.java,
                InteractionSystem::class.java,
                PhysicsSystem::class.java,
                PhysicsSynchronizationSystem::class.java,
                TransformTrackerSynchronizationSystem::class.java,
                LightPositionSynchronizationSystem::class.java,

                CameraTrackerSystem::class.java,
                RenderingSystem::class.java,
                HudSystem::class.java,
                UnfinishedPlaceablesHudSystem::class.java,

                HitResolverSystem::class.java,
                HitPointsSystem::class.java
//                    , PhysicsDebugSystem::class.java
        )
                .map { injector.getInstance(it) }
                .forEach { engine.addSystem(it) }

        val world = injector.getInstance(World::class.java)
        val lighting = injector.getInstance(Lighting::class.java)

        engine.addEntityListener(Family.one(PhysicsComponent::class.java).get(), PhysicsEntityListener(world, lighting))
    }

    private class PhysicsEntityListener(private val world: World, private val lighting: Lighting) : EntityListener {
        override fun entityAdded(entity: Entity) {
            markDirtyIfStatic(entity)
        }

        override fun entityRemoved(entity: Entity) {
            val physics = entity.physics
            world.destroyBody(physics.body)
            physics.additionalBodies.forEach { world.destroyBody(it) }
            markDirtyIfStatic(entity)
        }

        private fun markDirtyIfStatic(entity: Entity) {
            if (entity.physics.body.type == BodyDef.BodyType.StaticBody) {
                lighting.markDirty()
            }
        }
    }
}
