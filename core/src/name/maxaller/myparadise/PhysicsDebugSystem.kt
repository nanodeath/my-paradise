package name.maxaller.myparadise

import com.badlogic.ashley.core.EntitySystem
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer
import com.badlogic.gdx.physics.box2d.World
import com.google.inject.Inject
import com.badlogic.gdx.utils.Array as GdxArray

class PhysicsDebugSystem @Inject constructor(private val world: World,
                                             private val camera: OrthographicCamera) : EntitySystem() {
    private val renderer = Box2DDebugRenderer()
    private val bodies = GdxArray<Body>(false, 16)

    private companion object {
        val shapeRenderer = ShapeRenderer().apply {
            color = Color.WHITE
        }
    }

    override fun update(deltaTime: Float) {
        renderer.render(world, camera.combined)

        drawXAtOrigins()
    }

    private fun drawXAtOrigins() {
        world.getBodies(bodies)

        shapeRenderer.projectionMatrix = camera.combined
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line)
        bodies.forEach { body ->
            shapeRenderer.x(body.position, 0.25F)
        }
        shapeRenderer.end()
    }
}