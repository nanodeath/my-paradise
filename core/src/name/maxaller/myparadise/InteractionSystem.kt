package name.maxaller.myparadise

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.systems.IteratingSystem
import com.google.inject.Inject

class InteractionSystem @Inject constructor(private val assets: MyAssetManager): IteratingSystem(Family.all(InteractionRequestedComponent::class.java).get()) {
    override fun processEntity(entity: Entity, deltaTime: Float) {
        entity.remove(InteractionRequestedComponent::class.java)
        val placeable = entity.tryGet(PlaceableComponent)?.placeableItemType
        if (placeable != null) {
            if (placeable.visualAsset == GameAssets.WoodenDoor) {
                // door is closed, please open
                entity.remove(PlaceableComponent)
                entity.remove(VisualAssetComponent)
                entity.add(PlaceableComponent(Items.WoodenDoorOpen))
                val sprite = Items.WoodenDoorOpen.visualAsset.getSprite(assets.assetManager)
                entity.add(sprite.toComponent())
                entity.physics.body.fixtureList.forEach { it.isSensor = true }
            } else if (placeable.visualAsset == GameAssets.WoodenDoorOpen) {
                // door is open, please close
                entity.remove(PlaceableComponent)
                entity.remove(VisualAssetComponent)
                entity.add(PlaceableComponent(Items.WoodenDoor))
                entity.add(Items.WoodenDoor.visualAsset.getSprite(assets.assetManager).toComponent())
                entity.physics.body.fixtureList.forEach { it.isSensor = false }
            }
        }
    }
}

class LinkedInteractionSystem @Inject constructor() : IteratingSystem(Family.all(InteractionRequestedComponent::class.java, LinkedInteractionComponent::class.java).get()) {
    override fun processEntity(entity: Entity, deltaTime: Float) {
        val link = entity.get(LinkedInteractionComponent)
        link.other.add(InteractionRequestedComponent)
    }
}