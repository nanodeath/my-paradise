package name.maxaller.myparadise

import com.badlogic.ashley.systems.IntervalSystem
import com.google.inject.Inject
import com.google.inject.Singleton

/**
 * For some reason, the Box2D directional lights have a horizontal boundary, effectively marking an arbitrary point of darkness.
 *
 * Tracking in https://github.com/libgdx/box2dlights/issues/98
 */
@Singleton
class PeriodicLightingRelighter @Inject constructor(private val lighting: Lighting) : IntervalSystem(1F) {
    override fun updateInterval() {
        lighting.markDirectionalLightsDirty()
    }
}