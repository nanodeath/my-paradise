package name.maxaller.myparadise

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.EntitySystem
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.utils.ImmutableArray
import com.badlogic.gdx.math.Vector2
import com.google.inject.Inject

class DaylightSystem @Inject constructor() : EntitySystem() {
    private var currentTime = 0F // 0 == 9 AM, 12 == 9 PM
    private var timeSinceLastUpdate = Float.POSITIVE_INFINITY
    private val sunPolynomial = LagrangePolynomial(listOf(
            Vector2(0F, 0F), // Day starts with dawn
            Vector2(6F, -85F), // 4-hour period around noon should be close to high noon
            Vector2(HOUR_OF_DUSK / 2F, -90F), // Sun should be straight up at noon
            Vector2(HOUR_OF_DUSK, -180F) // Sun sets at dusk
    ))
    private val nightPolynomial = LagrangePolynomial(listOf(
            Vector2(HOUR_OF_DUSK, -180F), // Night starts with sun on horizon
            Vector2(HOURS_PER_DAY, -360F) // Then at the final hour, sun has come full circle (-360 == 0)
    ))

    private companion object {
        const val SECONDS_PER_DAY = 60F * 3 // seconds
        const val UPDATES_PER_SECOND = 1F // seconds
        const val HOUR_OF_DUSK = 16F // hours
        const val HOURS_PER_DAY = 24F // hours
    }

    private lateinit var families: ImmutableArray<Entity>

    override fun addedToEngine(engine: Engine) {
        families = engine.getEntitiesFor(Family.one(SunComponent::class.java, MoonComponent::class.java).get())
    }

    override fun update(deltaTime: Float) {
        currentTime += deltaTime
        timeSinceLastUpdate += deltaTime
        if (timeSinceLastUpdate >= UPDATES_PER_SECOND) {
            timeSinceLastUpdate = 0F
            val hour = calculateHour()
            updateEntities(hour)
            if (isDay(hour)) {
            }
        }
    }

    private fun calculateHour(): Float {
        val fractionThroughDay = (currentTime / SECONDS_PER_DAY) % 1
        val hour = fractionThroughDay * HOURS_PER_DAY
        return hour
    }

    private fun isDay(hour: Float) = hour >= 0 && hour < HOUR_OF_DUSK

    private fun updateEntities(hour: Float) {
        families.forEach { entity ->
            entity.tryGet(SunComponent)?.apply {
                val newDirection = if (isDay(hour)) {
                    sunPolynomial.valueAt(hour)
                } else {
                    nightPolynomial.valueAt(hour)
                }
                direction = newDirection
            }
        }
    }
}

