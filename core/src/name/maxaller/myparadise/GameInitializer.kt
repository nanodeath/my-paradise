package name.maxaller.myparadise

import box2dLight.ConeLight
import box2dLight.DirectionalLight
import box2dLight.PointLight
import box2dLight.RayHandler
import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.physics.box2d.*
import com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef
import com.badlogic.gdx.physics.box2d.joints.WeldJointDef
import com.google.inject.Inject
import com.google.inject.Singleton
import com.hi5dev.box2d_pexml.PEXML

@Singleton
class GameInitializer @Inject constructor(private val world: World,
                                          private val engine: Engine,
                                          private val assets: MyAssetManager,
                                          private val gameWorldGenerator: GameWorldGenerator,
                                          private val gameWorldBuilder: GameWorldBuilder,
                                          private val rayHandler: RayHandler,
                                          private val blockBuilder: BlockBuilder,
                                          private val lighting: Lighting) {
    private val players = engine.getEntitiesFor(Family.all(PlayerComponent::class.java, InventoryComponent::class.java).get())

    fun initialize() {
        val player = createPlayer()
        createTerrain()
        createSlime().apply {
            val playerPos = players.first().get(TransformComponent).position
            physics.body.setTransform(playerPos.to2d(), 0F)
        }
        createWeapon(WeaponDescription(WeaponType.WOODEN_CLUB, GameAssets.WoodenClub, 1F)).apply {
            physics.body.setTransform(player.physics.body.position.x + 10F, player.physics.body.position.y, 0F)
            dropToGround()
        }
        createWeapon(WeaponDescription(WeaponType.SHOVEL, GameAssets.Shovel, 1F)).apply {
            physics.body.setTransform(player.physics.body.position.x + 3F, player.physics.body.position.y, 0F)
            dropToGround()
        }
        createTrain(41, 3, null)

        createBackground()
        createLighting()
    }

    fun destroyEverything() {
        engine.removeAllEntities()
    }

    private fun createTerrain() {
        val world = gameWorldGenerator.generate(250, 25)
        gameWorldBuilder.build(world, offset = Vector2(0F, -20F), size = 1F)
    }

    fun createLighting() {
        RayHandler.useDiffuseLight(true)

        val color = Color(Color.WHITE).apply { a = 0.2F }
        val separation = 15F / 2F

        engine.addEntity(Entity().apply { add(SunComponent(-90F, 0F, createDirectionalLight(color))) })
        engine.addEntity(Entity().apply { add(SunComponent(-90F, -separation, createDirectionalLight(color))) })
        engine.addEntity(Entity().apply { add(SunComponent(-90F,  separation, createDirectionalLight(color))) })
        engine.addEntity(Entity().apply { add(SunComponent(-90F, -separation * 2, createDirectionalLight(color))) })
        engine.addEntity(Entity().apply { add(SunComponent(-90F,  separation * 2, createDirectionalLight(color))) })

        val moonColor = Color(Color.WHITE).apply { a = 0.03F }
        engine.addEntity(Entity().apply { add(MoonComponent(90F, createDirectionalLight(moonColor))) })
        engine.addEntity(Entity().apply { add(MoonComponent(90F - separation, createDirectionalLight(moonColor))) })
        engine.addEntity(Entity().apply { add(MoonComponent(90F + separation, createDirectionalLight(moonColor))) })
        engine.addEntity(Entity().apply { add(MoonComponent(90F - separation * 2, createDirectionalLight(moonColor))) })
        engine.addEntity(Entity().apply { add(MoonComponent(90F + separation * 2, createDirectionalLight(moonColor))) })

        val starsColor = Color(Color.WHITE).apply { a = 0.1F }
        engine.addEntity(Entity().apply {
            add(StarsComponent(-90F, createDirectionalLight(starsColor).apply {
                isXray = true
            }))
        })
    }

    private fun createDirectionalLight(color: Color): DirectionalLight {
        return DirectionalLight(rayHandler, 100, color, 0F).apply {
            isStaticLight = true
            setSoftnessLength(3F)
            setContactFilter(PhysicsSystem.CATEGORY_SCENERY, 0.toShort(), PhysicsSystem.MASK_LIGHTING)

            lighting.addStaticLight(this)
        }
    }

    fun createPlayer(): Entity {
        val entity = Entity().apply entity@ {
            add(TransformComponent(Vector2(20F, 8F)))
            val filter = Filter().apply {
                categoryBits = PhysicsSystem.CATEGORY_PLAYER
                maskBits = PhysicsSystem.MASK_PLAYER
            }

            fun createBody(): Body {
                val body = world.createBody(BodyDef().apply {
                    type = BodyDef.BodyType.DynamicBody
                    fixedRotation = true
                }).apply {
                    userData = this@entity
                }
                val density = 50F
                // bottom circle
                body.createFixture(CircleShape().apply {
                    radius = 0.5F
                    position = Vector2(0F, -0.5F)

                }, density / 2F).apply {
                    friction = 0.0F
                    filterData = filter
                }
                // middle box
                body.createFixture(PolygonShape().apply {
                    setAsBox(0.48F, 0.5F)
                }, density).apply {
                    filterData = filter
                }
                // top circle
                // Commenting for now, might put it back in later when it's a bit...shorter
//            body.createFixture(CircleShape().apply {
//                radius = 0.5F
//                position = Vector2(0F, 0.5F)
//            }, density / 2F)
                // feet
                body.createFixture(PolygonShape().apply {
                    val halfHeight = 0.05F
                    setAsBox(0.25F, halfHeight, Vector2(0F, -1F - halfHeight), 0F)
                }, 0F).apply {
                    filterData = filter
                    isSensor = true
                    userData = "feet"
                }
                body.setTransform(transform.position.to2d(), 0F)
                body.userData = this@entity
                return body
            }

            add(PhysicsComponent(createBody()))

            // Create arm
            fun createArm(): Body {
                val body = world.createBody(BodyDef().apply {
                    type = BodyDef.BodyType.DynamicBody
                }).apply {
                    // userData = ???
                }
                body.createFixture(PolygonShape().apply {
                    val lengthOfArm = 0.25F
//                    setAsBox(0.1F, lengthOfArm, Vector2(0F, -lengthOfArm * 0.95F), 0F)
                    setAsBox(lengthOfArm, 0.1F, Vector2(lengthOfArm * 0.95F, 0F), 0F)
                }, 50F).apply {
                    filterData = filter
                    isSensor = true
                }

                return body
            }
            fun totalMass(physicsComponent: PhysicsComponent): Float {
                return physicsComponent.body.mass + physicsComponent.additionalBodies.map { it.mass }.sum()
            }
            physics.manageBody(createArm().let { arm ->
                arm.setTransform(transform.position.to2d(), 0F)
                arm
            })
            println("total mass of player: ${totalMass(physics)} kg")
            physics.manageJoint(world.createJoint(RevoluteJointDef().apply {
                bodyA = this@entity.physics.body
                bodyB = physics.additionalBodies[0]
                localAnchorA.set(-0.25F, -0.125F)
                enableLimit = true
                lowerAngle = -100F.toRadians
                upperAngle = 45F.toRadians
//                enableMotor = true
//                motorSpeed = 30F.toRadians
//                maxMotorTorque = 10F
            }))

            add(PlayerComponent())

            add(CharacterAnimationComponent(
                    faceRight = assets.maleSpriteFaceRight(),
                    moveRight = assets.maleSpriteWalkRight(),
                    faceLeft = assets.maleSpriteFaceLeft(),
                    moveLeft = assets.maleSpriteWalkLeft()
            ))

            add(InventoryComponent().apply {
//                give(blockBuilder.createItem("wood"))
                give(blockBuilder.createItem("wooden_wall"), 30)
                give(Items.WoodenDoor, 3)
            })

            // Just for fun
            // Doesn't orient with the player :/
            val headlamp = ConeLight(rayHandler, 20, Color.WHITE, 10F, 0F, 0F, 0F, 20F)
            add(LightComponent(headlamp, 0F, 0.5F))
        }
        engine.addEntity(entity)
        return entity
    }

    fun createBackground() {
        engine.addEntity(Entity().apply {
            val position = Vector3(0F, 0F, 0F)
            position.z = -10000F
            add(TransformComponent(position, 0F, 2F))
            add(assets.backgroundTextureRegion().toComponent())
            add(UsesParallaxCamera(0.25F, 0.125F))
        })
    }

    fun createCraftingTable(): Entity {
        return Entity().apply {
            add(TransformComponent())
            add(TextureRegionComponent(assets.craftingTableTextureRegion()))
            add(CraftingComponent(CraftingComponentType.CRAFTING_TABLE))
            engine.addEntity(this)
        }
    }

    fun createTorch(): Entity {
        return Entity().apply {
            add(TransformComponent())
            add(assets.torchSprite().toComponent())
            val lightSource = PointLight(rayHandler, 100, Color.ORANGE, 10F, 0F, 0F).apply {
                val playerPosition = players.first().get(TransformComponent).position
                setPosition(playerPosition.x, playerPosition.y)
            }
            add(LightComponent(lightSource))
            engine.addEntity(this)
        }
    }

    fun createSlime(): Entity {
        return Entity().apply {
            add(TransformComponent())

            val filter = Filter().apply {
                categoryBits = PhysicsSystem.CATEGORY_CREATURE
                maskBits = PhysicsSystem.MASK_CREATURE
            }
            val body = world.createBody(BodyDef().apply {
                type = BodyDef.BodyType.DynamicBody
                fixedRotation = true
            })
            val totalRestitution = 0.5F
            body.createFixture(CircleShape().apply {
                radius = 0.5F
            }, 0.5F).apply {
                restitution = totalRestitution
                filterData = filter
            }
            body.createFixture(CircleShape().apply {
                radius = 0.25F
                position = Vector2(0F, -0.25F)
            }, 5F).apply {
                restitution = totalRestitution
                filterData = filter
            }
            body.userData = this

            add(PhysicsComponent(body))
            add(AnimationComponent(0.5F, assets.slimeSprites()))
            add(CreatureAiComponent.Bounce())

            val lightSource = PointLight(rayHandler, 5, Color.BLUE, 5F, 0F, 0F)
            add(LightComponent(lightSource))

            add(HitpointsComponent(20))

            engine.addEntity(this)
        }
    }

    fun createWeapon(weaponDescription: WeaponDescription): Entity {
        return Entity().apply {
            add(TransformComponent())
            val bodyDef = BodyDef().apply {
                type = BodyDef.BodyType.DynamicBody
            }
            val physicsFile = weaponDescription.baseWeapon.physicsFile
            val body = if (physicsFile != null) {
                val pexml = PEXML(Gdx.files.internal(physicsFile).file())
                pexml.createBody(weaponDescription.baseWeapon.physicsFileBodyName, world, bodyDef, 1.pixelsToMeters, 1.pixelsToMeters)
            } else {
                world.createBody(bodyDef).apply {
                    createFixture(PolygonShape().apply {
                        setAsBox(0.5F, 0.125F)
                    }, 10F)
                }
            }
            body.userData = this
            add(PhysicsComponent(body))
            add(SpriteComponent(weaponDescription.baseWeapon.getSprite(assets.assetManager)))
            addAsset(weaponDescription.baseWeapon)
            add(WeaponComponent(weaponDescription))

            engine.addEntity(this)
        }
    }

    fun createTrain(x: Int, y: Int, trainDescription: Any?): Entity {
        val pexml = PEXML(Gdx.files.internal("output/train_car_physics.xml").file())

        return Entity().apply {
            val width = 10

            val subentities = (0 until width).map { i ->
                val sectionType = if (i == 1 || i == width - 2) {
                    "wheel"
                } else "regular"
                Entity().apply {
                    add(TransformComponent(Vector2((x + i).toFloat(), y.toFloat() - 0.5F)))
                    val key = if (sectionType == "wheel") "train_car_wheel" else "train_car"
                    add(assets.getSprite("output/terrain.txt", key).toComponent())
                    val body = pexml.createBody(key, world, BodyDef().apply {
                        type = BodyDef.BodyType.StaticBody
                    }, 1.pixelsToMeters, 1.pixelsToMeters)
                    body.setTransform(transform.position.to2d(), 0F)
                    add(PhysicsComponent(body))
                }
            }
            subentities.forEach { engine.addEntity(it) }
            subentities.reduce { prev, curr ->
                world.createJoint(WeldJointDef().apply {
                    initialize(prev.physics.body, curr.physics.body, curr.physics.body.worldCenter)
                })
                curr
            }
            add(TrainCarsComponent(subentities))
        }
    }
}

private fun Entity.addAsset(asset: GameAssets.AbstractVisual) {
    add(VisualAssetComponent(asset))
    if (asset is GameAssets.Weapon) {
        add(WeaponAssetComponent(asset))
    }
}
