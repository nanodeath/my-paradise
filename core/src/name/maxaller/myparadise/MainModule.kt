package name.maxaller.myparadise

import box2dLight.RayHandler
import com.badlogic.ashley.core.Component
import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.physics.box2d.*
import com.google.inject.AbstractModule
import com.google.inject.Provides
import com.google.inject.Singleton

class MainModule : AbstractModule() {
    override fun configure() {
        binder().apply {
            requireAtInjectOnConstructors()
            requireExactBindingAnnotations()
        }

        bind(SpriteBatch::class.java).toInstance(SpriteBatch())
    }

    /**
     * Initialized in [MyParadiseGame.setupEngine].
     */
    @Provides @Singleton
    fun engine() = Engine()

    @Provides @Singleton
    fun camera(): OrthographicCamera {
        val viewportWidth = Gdx.graphics.width.pixelsToMeters
        val viewportHeight = Gdx.graphics.height.pixelsToMeters
        return OrthographicCamera().apply {
            setToOrtho(false, viewportWidth, viewportHeight)
            update()
        }
    }

    @GuiCam @Provides @Singleton
    fun guiCam(): OrthographicCamera = OrthographicCamera().apply {
        setToOrtho(false)
    }

    @Provides @Singleton
    fun rayHandler(world: World): RayHandler {
        val scale = 4 // 4 is the default scale. Larger numbers make for blobbier shadows, smaller for crisper shadows
        return RayHandler(world, Gdx.graphics.width / scale, Gdx.graphics.height / scale)
    }

    @Provides @Singleton
    fun world(engine: Engine): World {
        return World(Vector2(0F, -9.81F), true).apply {
            setContactListener(object : ContactListener {
                override fun beginContact(contact: Contact) {
                    val userDataA = contact.fixtureA.userData as? String
                    val userDataB = contact.fixtureB.userData as? String
                    // Handle feet-on-ground check
                    if (userDataA == "feet") {
                        putOnGround(contact.fixtureA, true)
                    } else if (userDataB == "feet") {
                        putOnGround(contact.fixtureB, true)
                    }
                    // Acquire item
                    val entityA = contact.fixtureA.body.userData as? Entity
                    val entityB = contact.fixtureB.body.userData as? Entity
//                    val b = entityA != null && entityA.has(PlayerComponent)
//                    val b2 = entityB != null && entityB.has(PlayerComponent)
//                    if (b || b2) {
//                        if (b && (entityB != null && !entityB.has(TerrainComponent))) {
//                            println("Player colliding with something")
//                        }
//
//                        if (b2 && (entityA != null && !entityA.has(TerrainComponent))) {
//                            println("Player colliding with something")
//                        }
//                    }
                    if (entityA != null && entityB != null) {
                        addCollision(entityA, entityB)
                        addCollision(entityB, entityA)
                    }
                }

                private fun addCollision(collider: Entity, collidee: Entity) {
                    collider.getOrAdd(CollidingWithComponent) { CollidingWithComponent() }.apply {
                        addCollision(collidee)
                    }
                }

                private fun putOnGround(fixture: Fixture, isOnGround: Boolean) {
                    val entity = fixture.body.userData as Entity
                    if (isOnGround) {
                        entity.get(PlayerComponent).groundCollisions++
                    } else {
                        entity.get(PlayerComponent).groundCollisions--
                    }
                }

                override fun endContact(contact: Contact) {
                    val userDataA = contact.fixtureA.userData as? String
                    val userDataB = contact.fixtureB.userData as? String
                    if (userDataA == "feet") {
                        putOnGround(contact.fixtureA, false)
                    } else if (userDataB == "feet") {
                        putOnGround(contact.fixtureB, false)
                    }


                    val entityA = contact.fixtureA.body.userData as? Entity
                    val entityB = contact.fixtureB.body.userData as? Entity
                    if (entityA != null && entityB != null) {
                        removeCollision(entityA, entityB)
                        removeCollision(entityB, entityA)
                    }
                }

                private fun removeCollision(collider: Entity, collidee: Entity) {
                    collider.tryGet(CollidingWithComponent)?.apply {
                        removeCollision(collidee)
                        if (!colliding) {
                            collider.remove(CollidingWithComponent::class.java)
                        }
                    }
                }

                override fun preSolve(contact: Contact?, oldManifold: Manifold?) {
                }

                override fun postSolve(contact: Contact, impulse: ContactImpulse) {
                    val entityA = contact.fixtureA.body.userData as? Entity
                    val entityB = contact.fixtureB.body.userData as? Entity
                    if (entityA != null && entityB != null) {
                        val other: Entity?
                        val weapon = if (entityA.has(WeaponComponent)) {
                            other = entityB
                            entityA
                        } else if (entityB.has(WeaponComponent)) {
                            other = entityA
                            entityB
                        } else {
                            other = null
                            null
                        }
                        if (weapon != null && impulse.count > 0) {
                            val hitThing = other?.has(HitpointsComponent) ?: false
                            if (hitThing) {
                                val forceVector = Vector2(impulse.normalImpulses[0], impulse.normalImpulses[1])
                                val mag = forceVector.len()
                                val damage = mag.toInt()
                                val effectiveDamage = if (damage > 5) {
                                    damage - 5
                                } else {
                                    0
                                }
                                if (effectiveDamage > 0) {
                                    other?.add(JustHitComponent(weapon, mag))
                                }
                            }
                            val hitTerrain = other?.has(TerrainComponent) ?: false
                            if (hitTerrain) {
                                val forceVector = Vector2(impulse.normalImpulses[0], impulse.normalImpulses[1])
                                val mag = forceVector.len()
                                other?.add(JustHitComponent(weapon, mag))
                            }
                        }
                        val (player, terrain) = getThem(entityA, entityB, { it -> it.has(PlayerComponent) }, { it -> it.has(BlockComponent) })
                        if (player != null && terrain != null) {
                            addCollision(player, terrain)
                        }
                    }
                }

                private fun getThem(entityA: Entity, entityB: Entity, predA: (Entity) -> Boolean, predB: (Entity) -> Boolean): Pair<Entity?, Entity?> {
                    return if (predA(entityA) && predB(entityB)) {
                        Pair(entityA, entityB)
                    } else if (predA(entityB) && predB(entityA)) {
                        Pair(entityB, entityA)
                    } else {
                        Pair(null, null)
                    }
                }
            })
        }
    }
}

class JustHitComponent(val weapon: Entity, val magnitude: Float) : Component {
    companion object : ComponentResolver<JustHitComponent>(JustHitComponent::class.java)
}

fun Camera.parallax(parallaxXSpeed: Float, parallaxYSpeed: Float): Matrix4 {
    update()
    val tmp = Vector3(position)
    tmp.x *= parallaxXSpeed
    tmp.y *= parallaxYSpeed
    val parallaxView = Matrix4().setToLookAt(tmp, Vector3(tmp).add(direction), up)
    return Matrix4(projection).mul(parallaxView)
}
