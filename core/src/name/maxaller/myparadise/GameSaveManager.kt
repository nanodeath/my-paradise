package name.maxaller.myparadise

import box2dLight.RayHandler
import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Family
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.physics.box2d.World
import com.badlogic.gdx.utils.TimeUtils
import com.esotericsoftware.kryo.Kryo
import com.esotericsoftware.kryo.Serializer
import com.esotericsoftware.kryo.io.Input
import com.esotericsoftware.kryo.io.Output
import com.google.inject.ImplementedBy
import com.google.inject.Inject
import com.google.inject.Singleton
import net.jpountz.lz4.LZ4BlockInputStream
import net.jpountz.lz4.LZ4BlockOutputStream
import java.io.File
import com.badlogic.gdx.utils.Array as GdxArray

@Singleton
class GameSaveManager @Inject constructor(private val playerControllerSystem: PlayerControllerSystem,
                                          private val saver: Saver,
                                          private val loader: Loader) {
    fun saveIfRequested() {
        if (playerControllerSystem.saveRequested) {
            playerControllerSystem.saveRequested = false
            saver.saveGame()
        }
    }

    fun loadIfRequested() {
        if (playerControllerSystem.loadRequested) {
            playerControllerSystem.loadRequested = false
            loader.loadGame()
        }
    }
}

@ImplementedBy(KryoSaver::class)
interface Saver {
    fun saveGame()
}

@ImplementedBy(KryoLoader::class)
interface Loader {
    fun loadGame()
}

object KryoHolder {
    val kryo = Kryo()
    val compressed = true

    init {
        kryo.apply {
            register(TransformComponent::class.java, object : Serializer<TransformComponent>() {
                override fun write(kryo: Kryo, output: Output, component: TransformComponent) {
                    kryo.writeObject(output, component.position)
                    kryo.writeObject(output, component.angleRadians)
                    kryo.writeObject(output, component.scale)
                }

                override fun read(kryo: Kryo, input: Input, type: Class<TransformComponent>): TransformComponent {
                    return TransformComponent(
                            kryo.readObject(input, Vector3::class.java),
                            kryo.readObject(input, Float::class.java),
                            kryo.readObject(input, Float::class.java)
                    )
                }
            })

            register(TerrainWorldDescription::class.java, object : Serializer<TerrainWorldDescription>() {
                override fun write(kryo: Kryo, output: Output, component: TerrainWorldDescription) {
                    kryo.writeObject(output, component.dimensions)
                    kryo.writeObject(output, component.offset)
                    kryo.writeObject(output, component.size)
                }

                override fun read(kryo: Kryo, input: Input, type: Class<TerrainWorldDescription>): TerrainWorldDescription {
                    return TerrainWorldDescription(
                            kryo.readObject(input, Vector2::class.java),
                            kryo.readObject(input, Vector2::class.java),
                            kryo.readObject(input, Float::class.java)
                    )
                }
            })

            register(TerrainComponent::class.java, object : Serializer<TerrainComponent>() {
                override fun write(kryo: Kryo, output: Output, component: TerrainComponent) {
                    kryo.writeObject(output, component.instance.terrain.name)
                    kryo.writeObject(output, component.x)
                    kryo.writeObject(output, component.y)
                }

                override fun read(kryo: Kryo, input: Input, type: Class<TerrainComponent>): TerrainComponent {
                    return TerrainComponent(
                            /*kryo.readObject(input, String::class.java)*/TerrainInstance(Terrain(), TerrainSubtype()),
                            kryo.readObject(input, Int::class.java),
                            kryo.readObject(input, Int::class.java)
                    )
                }
            })

            register(CraftingComponent::class.java, object : Serializer<CraftingComponent>() {
                override fun write(kryo: Kryo, output: Output, component: CraftingComponent) {
                    kryo.writeObject(output, component.type)
                }

                override fun read(kryo: Kryo, input: Input, type: Class<CraftingComponent>): CraftingComponent {
                    return CraftingComponent(
                            kryo.readObject(input, CraftingComponentType::class.java)
                    )
                }
            })
        }
    }
}

@Singleton
class KryoSaver @Inject constructor(private val engine: Engine) : Saver {
    private val kryo = KryoHolder.kryo
    private val compressed = KryoHolder.compressed

    override fun saveGame() {
        val now = TimeUtils.millis()
        val file = File("out.save")
        file.outputStream().buffered().let { os -> if (compressed) LZ4BlockOutputStream(os) else os }.let { Output(it) }.use { output ->
            writeVersion(output, 1)
            savePlayer(output)
            saveTerrain(output)
            saveCraftingObjects(output)
        }
        val finished = TimeUtils.millis()
        println("Took ${finished - now}ms to generate ${file.length() / 1024} KB")
    }

    private fun writeVersion(output: Output, version: Int) {
        kryo.writeObject(output, version)
    }

    private fun savePlayer(output: Output) {
        val players = engine.getEntitiesFor(Family.all(TransformComponent::class.java, PlayerComponent::class.java).get())
        val size = players.size()
        kryo.writeObject(output, size)
        players.forEach { player ->
            kryo.writeObject(output, player.get(PlayerComponent))
            kryo.writeObject(output, player.get(TransformComponent))
        }
    }

    private fun saveTerrain(output: Output) {
        val terrainWorldDescription = engine.getEntitiesFor(Family.all(TerrainWorldDescription::class.java).get()).first().get(TerrainWorldDescription)
        kryo.writeObject(output, terrainWorldDescription)
        val terrain = engine.getEntitiesFor(Family.all(TerrainComponent::class.java).get())
        kryo.writeObject(output, terrain.size())
        terrain.forEach { t ->
            val terrainComponent = t.get(TerrainComponent)
            kryo.writeObject(output, terrainComponent)
        }
        println("Saved ${terrain.size()} terrain tiles")
    }

    private fun saveCraftingObjects(output: Output) {
        val craftingObjects = engine.getEntitiesFor(Family.all(CraftingComponent::class.java).get())
        val size = craftingObjects.size()
        kryo.writeObject(output, size)
        craftingObjects.forEach { craftingObject ->
            kryo.writeObject(output, craftingObject.get(TransformComponent))
            kryo.writeObject(output, craftingObject.get(CraftingComponent))
        }
    }
}

@Singleton
class KryoLoader @Inject constructor(private val engine: Engine,
                                     private val world: World,
                                     private val gameWorldBuilder: GameWorldBuilder,
                                     private val gameInitializer: GameInitializer,
                                     private val rayHandler: RayHandler) : Loader {
    private val kryo = KryoHolder.kryo
    private val compressed = KryoHolder.compressed

    override fun loadGame() {
        engine.removeAllEntities()
        rayHandler.removeAll()
        world.removeAllBodies()

        val now = TimeUtils.millis()
        val file = File("out.save")
        file.inputStream().buffered().let { if (compressed) LZ4BlockInputStream(it) else it }.let(::Input).use { input ->
            readAndAssertVersion(input, 1)
            loadPlayer(input)
            loadTerrain(input)
            loadCraftingObjects(input)
            gameInitializer.createBackground()
            gameInitializer.createLighting()
        }
        val finished = TimeUtils.millis()
        println("Took ${finished - now}ms to load ${file.length() / 1024} KB")
    }

    private fun readAndAssertVersion(input: Input, expectedVersion: Int) {
        val actualVersion = kryo.readObject(input, Int::class.java)
        check(expectedVersion == actualVersion) { "Was expecting version $expectedVersion but got $actualVersion" }
    }

    private fun loadPlayer(input: Input) {
        val playerCount = kryo.readObject(input, Int::class.java)
        for (i in 1..playerCount) {
            val playerComponent = kryo.readObject(input, PlayerComponent::class.java).apply {
                groundCollisions = 0
            }
            val transformComponent = kryo.readObject(input, TransformComponent::class.java)
            gameInitializer.createPlayer().apply {
                add(playerComponent)
                physics.body.setTransform(transformComponent.position.to2d(), 0F)
            }
        }
    }

    private fun loadTerrain(input: Input) {
        val terrainWorldDescription = kryo.readObject(input, TerrainWorldDescription::class.java)
        val width = terrainWorldDescription.dimensions.x.toInt()
        val height = terrainWorldDescription.dimensions.y.toInt()
        val array = Array(width, { Array<String?>(height, { null }) })
        val terrainCount = kryo.readObject(input, Int::class.java)
        for (i in 1..terrainCount) {
            val datum = kryo.readObject(input, TerrainComponent::class.java)
            array[datum.x][datum.y] = datum.instance.terrain.name
        }
        gameWorldBuilder.build(array, terrainWorldDescription.offset, terrainWorldDescription.size)
    }

    private fun loadCraftingObjects(input: Input) {
        val size = kryo.readObject(input, Int::class.java)
        for (i in 1..size) {
            val transformComponent = kryo.readObject(input, TransformComponent::class.java)
            val craftingComponent = kryo.readObject(input, CraftingComponent::class.java)
            gameInitializer.createCraftingTable().apply {
                add(transformComponent)
                add(craftingComponent)
            }
        }
    }
}
