package name.maxaller.myparadise

import box2dLight.DirectionalLight
import box2dLight.Light
import com.google.inject.Inject
import com.google.inject.Singleton
import com.badlogic.gdx.utils.Array as GdxArray

@Singleton
class Lighting @Inject constructor() {
    private var dirty = Dirty.NONE
    private val staticLights = GdxArray<Light>(false, 16)

    fun addStaticLight(light: Light) {
        require(light.isStaticLight) { "Expected $light to be static" }
        staticLights.add(light)
    }

    fun removeStaticLight(light: Light) {
        staticLights.removeValue(light, true)
    }

    fun markDirty() {
        dirty = Dirty.ALL
    }

    fun markDirectionalLightsDirty() {
        if (dirty != Dirty.ALL) {
            dirty = Dirty.DIRECTIONAL
        }
    }

    fun forceStaticLightUpdateIfNecessary() {
        if (dirty != Dirty.NONE) {
            staticLights.forEach { light ->
                assert(light.isStaticLight) { "Light changed from static to non-static! $light" }
                if (dirty == Dirty.ALL || (dirty == Dirty.DIRECTIONAL && light is DirectionalLight)) {
                    light.isStaticLight = light.isStaticLight
                }
            }
            dirty = Dirty.NONE
        }
    }

    private enum class Dirty {
        NONE, DIRECTIONAL, ALL
    }
}