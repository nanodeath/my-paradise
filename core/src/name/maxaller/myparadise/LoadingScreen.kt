package name.maxaller.myparadise

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.ScreenAdapter
import com.badlogic.gdx.assets.AssetLoaderParameters
import com.badlogic.gdx.assets.loaders.*
import com.badlogic.gdx.audio.Music
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.GlyphLayout
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.utils.I18NBundle
import com.badlogic.gdx.utils.TimeUtils
import com.google.inject.Inject
import com.moandjiezana.toml.Toml
import java.util.*

class LoadingScreen @Inject constructor(private val assets: MyAssetManager, private val spriteBatch: SpriteBatch,
                                        private val terrainTypes: TerrainTypes) : ScreenAdapter() {
    private val setupStrings = "strings/SetupStrings"
    private val font = BitmapFont().apply {
        color = Color.WHITE
        data.setScale(2F)
    }

    private var musicLoaded = 0
    private var musicRequested = 0
    private var atlasesLoaded = 0
    private var atlasesRequested = 0
    private var skinsLoaded = 0
    private var skinsRequested = 0
    private var texturesLoaded = 0
    private var texturesRequested = 0

    private var soundsLoaded = 0
    private var soundsRequested = 0
    private var stringsLoaded = 0
    private var stringsRequested = 0

    private var totalLoaded = 0
    private var totalRequested = 0

    private var startedAt: Long = -1L
    private var finishedAt: Long = -1L

    private val loadingTextWidth: Float
    private val loadingTextHeight: Float
    private val setupBundle: I18NBundle

    var doneLoading = false
        private set

    override fun show() {
        startedAt = TimeUtils.millis()
        loadSkin("skin/uiskin.json")
        loadTextureAtlas("output/creatures_items.txt")
        loadTextureAtlas("output/terrain.txt")
        loadTexture("output/bg_jungle_flat.png")
        loadMusic("originals/audio/forest.mp3")
        update()
    }

    init {
        assets.assetManager.load(setupStrings, I18NBundle::class.java, I18NBundleLoader.I18NBundleParameter(Locale.US))
        assets.assetManager.finishLoadingAsset(setupStrings)
        setupBundle = assets.assetManager.get(setupStrings)
        val glyphLayout = GlyphLayout().apply {
            setText(font, setupBundle.format("gui.loading", 10, 10))
        }
        loadingTextWidth = glyphLayout.width
        loadingTextHeight = glyphLayout.height
        terrainTypes.apply {
//            registerTerrainType(Blocks.Dirt.name, Blocks.Dirt, GameWorldGenerator.TerrainType.DIRT)
//            registerTerrainType(Blocks.Wood.name, Blocks.Wood, GameWorldGenerator.TerrainType.WOOD)
//            registerTerrainType(Blocks.WoodenWall.name, Blocks.WoodenWall, GameWorldGenerator.TerrainType.WOODEN_WALL)
//            registerTerrainType(Blocks.WoodenWallCorner.name, Blocks.WoodenWallCorner, GameWorldGenerator.TerrainType.WOODEN_WALL_CORNER)
//            registerTerrainType(Blocks.WoodenWallT.name, Blocks.WoodenWallT, GameWorldGenerator.TerrainType.WOODEN_WALL_T)
//            registerTerrainType(Blocks.WoodenWallX.name, Blocks.WoodenWallX, GameWorldGenerator.TerrainType.WOODEN_WALL_X)
//            registerTerrainType(Blocks.WoodenWallBG.name, Blocks.WoodenWallBG, GameWorldGenerator.TerrainType.WOODEN_WALL_BG)
        }

        val terrainData = Gdx.files.internal("data/terrain.toml")
        val foo = Toml().read(terrainData.reader()).to(TerrainArray::class.java)
        for (terrain in foo.terrain) {
            terrainTypes.registerTerrainType(terrain.name, terrain)
        }
        println(foo)
    }

    class TerrainArray {
        var terrain: List<Terrain> = emptyList()
    }

    override fun render(delta: Float) {
        Gdx.gl.glClearColor(0f, 0f, 0f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT or GL20.GL_DEPTH_BUFFER_BIT)

        update()
        spriteBatch.begin()
        val text = setupBundle.format("gui.loading", totalLoaded, totalRequested)
        spriteBatch
        font.draw(spriteBatch, text, Gdx.graphics.width / 2F - loadingTextWidth / 2, Gdx.graphics.height / 2F + loadingTextHeight / 2F)
        spriteBatch.end()
    }

    override fun resize(width: Int, height: Int) {
        spriteBatch.projectionMatrix.setToOrtho2D(0F, 0F, Gdx.graphics.width.toFloat(), Gdx.graphics.height.toFloat())
    }

    private fun update() {
        val wasDoneLoading = doneLoading
        totalRequested = musicRequested + soundsRequested + texturesRequested + atlasesRequested + stringsRequested + skinsRequested
        doneLoading = assets.assetManager.update()
        totalLoaded = musicLoaded + soundsLoaded + texturesLoaded + atlasesLoaded + stringsLoaded + skinsLoaded
        if (!wasDoneLoading && doneLoading) {
            finishedAt = TimeUtils.millis()
            println("DONE LOADING (took ${finishedAt - startedAt}ms)")
        }
    }

    private fun loadSkin(filename: String) {
        skinsRequested++
        val start = TimeUtils.millis()
        assets.assetManager.load(filename, Skin::class.java, SkinLoader.SkinParameter().apply {
            loadedCallback = AssetLoaderParameters.LoadedCallback { a, f, t ->
                println("Skin loaded: $filename at ${TimeUtils.millis() - start}ms")
                assets.skin = assets.assetManager.get(filename, Skin::class.java)
                skinsLoaded++
            }
        })
    }

    private fun loadMusic(filename: String) {
        musicRequested++
        val start = TimeUtils.millis()
        assets.assetManager.load(filename, Music::class.java, MusicLoader.MusicParameter().apply {
            loadedCallback = AssetLoaderParameters.LoadedCallback { a, f, t ->
                println("Music loaded: $filename at ${TimeUtils.millis() - start}ms")
                musicLoaded++
            }
        })
    }

    private fun loadTextureAtlas(filename: String) {
        atlasesRequested++
        val start = TimeUtils.millis()
        assets.assetManager.load(filename, TextureAtlas::class.java, TextureAtlasLoader.TextureAtlasParameter().apply {
            loadedCallback = AssetLoaderParameters.LoadedCallback { a, f, t ->
                println("Atlas loaded: $filename at ${TimeUtils.millis() - start}ms")
                atlasesLoaded++
            }
        })
    }

    private fun loadTexture(filename: String) {
        texturesRequested++
        val start = TimeUtils.millis()
        assets.assetManager.load(filename, Texture::class.java, TextureLoader.TextureParameter().apply {
            loadedCallback = AssetLoaderParameters.LoadedCallback { a, f, t ->
                println("Texture loaded: $filename at ${TimeUtils.millis() - start}ms")
                texturesLoaded++
            }
        })
    }
}