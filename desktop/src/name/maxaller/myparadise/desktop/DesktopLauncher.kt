package name.maxaller.myparadise.desktop

import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import name.maxaller.myparadise.MyParadiseGame

object DesktopLauncher {
    @JvmStatic fun main(arg: Array<String>) {
        val config = LwjglApplicationConfiguration().apply {
            width = 1440
            height = 900
            vSyncEnabled = true
        }
        LwjglApplication(MyParadiseGame(), config)
    }
}
