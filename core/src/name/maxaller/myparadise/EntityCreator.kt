package name.maxaller.myparadise

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.BodyDef
import com.badlogic.gdx.physics.box2d.Filter
import com.badlogic.gdx.physics.box2d.PolygonShape
import com.badlogic.gdx.physics.box2d.World
import com.google.inject.Inject

class EntityCreator @Inject constructor(private val engine: Engine, private val world: World, private val assets: MyAssetManager,
                                        private val terrainTypes: TerrainTypes) {

    fun createItem(name: String): Entity {
        println("CREATING $name")
        return when (name) {
            "grass_item" -> {
                createGrass(0, 0, isItem = true)
            }
            else -> throw IllegalArgumentException(name)
        }
    }

    fun getImage(name: String): Sprite {
        return when (name) {
            "grass_item" -> {
                assets.getSprite("output/terrain.txt", "grass")
            }
            else -> throw IllegalArgumentException(name)
        }
    }

    fun createGrass(x: Int, y: Int, isItem: Boolean = false): Entity {
        val scale = if (isItem) 0.5F else 1F
        val cat = if (isItem) PhysicsSystem.CATEGORY_ITEM else PhysicsSystem.CATEGORY_HITTABLE
        val bodyType = if (isItem) BodyDef.BodyType.DynamicBody else BodyDef.BodyType.StaticBody

        return Entity().apply {
            add(TransformComponent().apply {
                position.set(x.toFloat(), y.toFloat(), 0F)
                this.scale = scale
            })
            add(assets.getSprite(GameAssets.TERRAIN_ATLAS, "grass").toComponent())

            val filter = Filter().apply {
                categoryBits = cat
            }
            val body = world.createBody(BodyDef().apply {
                type = bodyType
            })
            body.userData = this
            body.createFixture(PolygonShape().apply {
                val size = 1F * scale
                setAsBox(size / 2F, size / 2F)
            }, 0F).apply {
                filterData = filter
            }
            body.setTransform(Vector2(x.toFloat(), y.toFloat()), 0F)

            add(PhysicsComponent(body))
            if (!isItem) {
                add(HitpointsComponent(1))
                add(ItemDropsComponent("grass"))
            } else {
//                add(BlockComponent(terrainTypes["dirt"]))
                add(ItemTypeComponent(Items.GrassItem))
            }
            engine.addEntity(this)
        }
    }

    fun createGrassBed(): Entity {
        return Entity().apply {
            add(TransformComponent())
            add(assets.getSprite(GameAssets.TERRAIN_ATLAS, "grass_bed").toComponent())
            engine.addEntity(this)
        }
    }

}
