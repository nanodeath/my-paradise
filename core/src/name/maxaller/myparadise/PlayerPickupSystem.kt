package name.maxaller.myparadise

import com.badlogic.ashley.core.Component
import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.Filter
import com.badlogic.gdx.physics.box2d.World
import com.badlogic.gdx.physics.box2d.joints.WeldJointDef
import com.google.inject.Inject

class PlayerPickupSystem @Inject constructor(private val itemEquipper: ItemEquipper) : IteratingSystem(Family.all(PlayerComponent::class.java, CollidingWithComponent::class.java).get()) {
    override fun processEntity(entity: Entity, deltaTime: Float) {
        val collidingWith = entity.get(CollidingWithComponent)
        val weapon = collidingWith.collisions.find { it.has(WeaponComponent) }
        if (weapon != null) {
            println("Player picking up weapon")
            entity.tryGet(InventoryComponent)?.let { inventory ->
                inventory.give(weapon)
                engine.removeEntity(weapon)
                itemEquipper.equipWeapon(entity, weapon)
            }
        }
        val block = collidingWith.collisions.find { it.has(BlockComponent) }
        if (block != null) {
            println("Player picking up block")
            entity.tryGet(InventoryComponent)?.let { inventory ->
                engine.removeEntity(block)
                collidingWith.removeCollision(block)
                inventory.give(block)
            }
        }
        val item = collidingWith.collisions.find { it.has(ItemTypeComponent) }
        if (item != null) {
            println("Player picking up an item")
            entity.tryGet(InventoryComponent)?.let { inventory ->
                engine.removeEntity(item)
                collidingWith.removeCollision(item)
                inventory.giveOrStack(item.get(ItemTypeComponent).itemType, 1)
            }
        }
    }
}

class ItemEquipper @Inject constructor(private val world: World, private val engine: Engine,
                                       private val gameInitializer: GameInitializer) {
    fun equipWeapon(wielder: Entity, weapon: Entity) { // entity must be something existing in world space
        unequipWeapon(wielder)

        val item = if (weapon.has(WeaponComponent)) {
            gameInitializer.createWeapon(weapon.get(WeaponComponent).weaponDescription).apply {
                physics.body.setTransform(wielder.physics.body.position.x, wielder.physics.body.position.y, 0F)
            }
        } else {
            engine.addEntity(weapon)
            weapon
        }
        wielder.get(PlayerComponent).equippedItem = item
        val filter = Filter().apply {
            categoryBits = PhysicsSystem.CATEGORY_PLAYER
            maskBits = PhysicsSystem.MASK_PLAYER_WEAPON
        }
        item.physics.body.fixtureList.forEach { it.filterData = filter }
        val joint = world.createJoint(WeldJointDef().apply {
            val arm = wielder.physics.additionalBodies[0]
            initialize(arm, item.physics.body, arm.getWorldPoint(Vector2(0.5F, 0F)))
            localAnchorB.set(-0.5F, 0F)
            referenceAngle = 90F.toRadians
        })
        item.physics.manageJoint(joint)
    }

    fun unequipWeapon(wielder: Entity) {
        val previousWeapon = wielder.get(PlayerComponent).equippedItem
        if (previousWeapon != null) {
            wielder.get(PlayerComponent).equippedItem = null
            engine.removeEntity(previousWeapon) // it's in their inventory anyway
        }
    }
}

class TransformTracksComponent(val entity: Entity, val xOffset: Float, val yOffset: Float) : Component {
    companion object : ComponentResolver<TransformTracksComponent>(TransformTracksComponent::class.java)
}

class TransformTrackerSynchronizationSystem @Inject constructor() : IteratingSystem(Family.all(TransformComponent::class.java, TransformTracksComponent::class.java).get()) {
    override fun processEntity(entity: Entity, deltaTime: Float) {
        val tracker = entity.get(TransformTracksComponent)
        val target = tracker.entity.transform.position
        if (entity.has(PhysicsComponent)) {
            val body = entity.physics.body
            body.setTransform(target.x + tracker.xOffset, target.y + tracker.yOffset, body.angle)
        } else {
            entity.get(TransformComponent).position.set(target)
        }
    }
}
