package name.maxaller.myparadise

import com.badlogic.gdx.math.Vector2
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class LagrangePolynomialTest {
    @Test
    fun test(){
        val points = listOf(Vector2(0F, 180F), Vector2(6F, 95F), Vector2(8F, 90F), Vector2(16F, 0F))
        assertThat(LagrangePolynomial(points).valueAt(0F)).isEqualTo(180F)

        assertThat(LagrangePolynomial(points).valueAt(2F)).isBetween(130F, 135F)
        assertThat(LagrangePolynomial(points).valueAt(4F)).isBetween(105F, 110F)
        assertThat(LagrangePolynomial(points).valueAt(6F)).isEqualTo(95F)

        assertThat(LagrangePolynomial(points).valueAt(8F)).isEqualTo(90F)

        assertThat(LagrangePolynomial(points).valueAt(10F)).isEqualTo(85F)
        assertThat(LagrangePolynomial(points).valueAt(12F)).isBetween(70F, 75F)
        assertThat(LagrangePolynomial(points).valueAt(14F)).isBetween(45F, 50F)

        assertThat(LagrangePolynomial(points).valueAt(16F)).isEqualTo(0F)
//        var previous = Float.POSITIVE_INFINITY
//        val stepSize = 0.1F
//        var i = 0F
//        while (i < 16F) {
//            val current = LagrangePolynomial(points).valueAt(i)
//            assertThat(current).isLessThanOrEqualTo(previous)
//            previous = current
//            i += stepSize
//        }
//        for (0  16 step stepSize)
    }
}