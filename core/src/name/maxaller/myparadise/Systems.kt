package name.maxaller.myparadise

import com.badlogic.ashley.core.EntitySystem
import com.google.inject.Inject

data class SystemClasses @Inject constructor(val classes: List<Class<out EntitySystem>>)
data class Systems @Inject constructor(val entitySystems: List<EntitySystem>)