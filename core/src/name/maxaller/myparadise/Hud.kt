package name.maxaller.myparadise

import com.badlogic.ashley.core.Component
import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.utils.ImmutableArray
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputAdapter
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.ui.Stack
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.Scaling
import com.badlogic.gdx.utils.viewport.FitViewport
import com.badlogic.gdx.utils.viewport.Viewport
import com.google.inject.Inject
import com.google.inject.Singleton
import java.util.*

@Singleton
class Hud @Inject constructor(
        spriteBatch: SpriteBatch,
        private @GuiCam val cam: OrthographicCamera,
        private val assets: MyAssetManager,
        private val gameInitializer: GameInitializer,
        private val itemEquipper: ItemEquipper,
        private val blockBuilder: BlockBuilder,
        private val worldCam: OrthographicCamera,
        private val entityCreator: EntityCreator
) : InputAdapter() {
    private val viewport: Viewport = FitViewport(cam.viewportWidth, cam.viewportHeight, cam)
    val stage = Stage(viewport, spriteBatch)
    private val skin = assets.skin
    lateinit private var players: ImmutableArray<Entity>
    lateinit private var engine: Engine

    private lateinit var screenTable: ScreenTable

    fun setEngine(engine: Engine) {
        this.engine = engine
        players = engine.getEntitiesFor(Family.all(PlayerComponent::class.java, InventoryComponent::class.java).get())

        screenTable = ScreenTable()
        screenTable.create()
    }

    private val table: Table

    init {
//        stage.setDebugAll(true)
        table = Table(skin)
        stage.addActor(table)
    }

    inner class ScreenTable {
        private val cells = ArrayList<Cell<*>>()
        private var dirty = false
        private var lastModificationCount = -1

        fun create() {
            createQuickBar()
            dirty = true
        }

        private fun createQuickBar() {
            stage.addActor(Table(skin).apply {

                val widthInColumns = 8
                val size = 32
                setBackground("default-pane")
                row().width(size.toFloat()).height(size.toFloat()).pad(size.toFloat() / 10F)
                for (x in 1..widthInColumns) {
                    this@ScreenTable.cells.add(add())
                }
                pack()
                centerOnScreen(x = true, y = false)
                makeSolid()

                addListener(object : ClickListener() {
                    override fun touchUp(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int) {
                        super.touchUp(event, x, y, pointer, button)
                        val itemStack = event.target.findUserObject(Entity::class.java)
                        if (itemStack != null) {
                            println("itemStack!")
                            itemStack.tryGet(BlockComponent)?.let { comp ->
                                println(" is block")
                                val block = blockBuilder.createDetachedBlock(comp.terrain)
                                itemEquipper.equipWeapon(players.first(), block)
                            }
                            itemStack.tryGet(WeaponComponent)?.let { comp ->
                                itemEquipper.equipWeapon(players.first(), itemStack)
                            }
                            itemStack.tryGet(EntityInInventory)?.let { comp ->
                                itemEquipper.equipWeapon(players.first(), comp.entity)
                            }
                            itemStack.tryGet(ItemTypeComponent)?.itemType?.let { itemType ->
                                println("Clicked itemType is $itemType")
                                if (itemType is PlaceableItemType) {
                                    val block = blockBuilder.createDetachedPlaceable(itemType)
                                    itemEquipper.equipWeapon(players.first(), block)
                                }
                            }
                        }
                    }
                })
            })
        }

        fun update() {
            if (players.size() > 0) {
                val currentModificationCount = players.first().get(InventoryComponent).modificationCount
                if (lastModificationCount != currentModificationCount) {
                    dirty = true
                    lastModificationCount = currentModificationCount
                }
            }
        }

        fun draw() {
            // TODO Disabling dirty-checking is horribly inefficient here :/
//            if (dirty) {
            if (players.size() == 0) {
                return
            }
//                dirty = false
            val inventory = players.first().get(InventoryComponent)
            val items = inventory.items

            cells.forEachIndexed { idx, cell ->
                val itemStack = if (idx < items.size) items[idx] else null
                cell.clearActor()
                if (itemStack != null) {
                    cell.setActor(Stack().apply stack@ {
                        val textureRegion = ItemOracle.getTextureRegionFor(itemStack, assets)
                        if (textureRegion is Sprite) {
                            add(Image(SpriteDrawable(textureRegion), Scaling.fit))
                        } else {
                            add(Image(TextureRegionDrawable(textureRegion), Scaling.fit))
                        }
                        addAmountOverlay(itemStack)
                        userObject = itemStack
                    })
                }
            }

//            }
        }
    }

    var currentMenu = Menu.NONE

    enum class Menu {
        NONE, CONTEXT, INVENTORY
    }

    fun toggleContextMenu() {
        table.clear()
        if (currentMenu != Menu.CONTEXT) {
            currentMenu = Menu.CONTEXT
            table.apply {
                setBackground("default-pane")
                row().width(400F).expandX()
                add(TextButton("Click Me!", skin).apply {
                    addListener(object : ChangeListener() {
                        override fun changed(event: ChangeEvent, actor: Actor) {
                            println("Clicked!")
                        }
                    })
                }).maxWidth(100F)
                pack()
                centerOnScreen()
                makeSolid()
            }
        } else {
            currentMenu = Menu.NONE
        }
        table.isVisible = currentMenu != Menu.NONE
        table.pack()
    }

    fun toggleInventory() {
        table.clear()
        if (currentMenu != Menu.INVENTORY) {
            currentMenu = Menu.INVENTORY
            drawInventory()
        } else {
            currentMenu = Menu.NONE
        }
        table.isVisible = currentMenu != Menu.NONE
        table.pack()
    }

    private var placing: Placement? = null

    override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        return if (placing != null) {
            placing?.install()
            placing = null
            true
        } else {
            false
        }
    }

    private fun drawInventory() {
        val widthInColumns = 8
        val heightInRows = 5
        val size = 32
        val items = players.first().get(InventoryComponent).items
        table.apply {
            setBackground("default-pane")
            add(Table(skin).apply {
                add(Label("Inventory", skin)).colspan(widthInColumns)
                row()
                var idx = 0
                for (y in 1..heightInRows) {
                    row().width(size.toFloat()).height(size.toFloat()).pad(size.toFloat() / 10F)
                    for (x in 1..widthInColumns) {
                        val itemStack = if (idx < items.size) items[idx] else null
                        if (itemStack != null) {
                            add(Stack().apply stack@ {
                                val textureRegion = ItemOracle.getTextureRegionFor(itemStack, assets)
                                if (textureRegion is Sprite) {
                                    add(Image(SpriteDrawable(textureRegion), Scaling.fit))
                                } else {
                                    add(Image(TextureRegionDrawable(textureRegion), Scaling.fit))
                                }
                                addAmountOverlay(itemStack)
                                addListener(object : ClickListener() {
                                    override fun touchUp(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int) {
                                        super.touchUp(event, x, y, pointer, button)
                                        val eventItemStack = event.target.findUserObject(Entity::class.java)
                                        if (eventItemStack != null) {
                                            eventItemStack.tryGet(BlockComponent)?.let { comp ->
                                                val block = blockBuilder.createDetachedBlock(comp.terrain)
                                                itemEquipper.equipWeapon(players.first(), block)
                                            }
                                            eventItemStack.tryGet(ItemTypeComponent)?.itemType?.let { itemType ->
                                                println("Clicked itemType is $itemType")
                                                if (itemType is PlaceableItemType) {
                                                    val block = blockBuilder.createDetachedPlaceable(itemType)
                                                    itemEquipper.equipWeapon(players.first(), block)
                                                }
                                            }
                                        }
                                    }
                                })
                            })
                        } else {
                            add()
                        }
                        idx++
                    }
                }
            })
            add(Table(skin).apply {
                add(Label("Crafting", skin))
                row()
                add(TextButton("Crafting Table", skin).apply {
                    addChangeListener {
                        toggleInventory()
                        val craftingTable = gameInitializer.createCraftingTable()
                        val position = Vector3(players.first().get(TransformComponent).position)
                        position.y -= 0.5F
                        craftingTable.get(TransformComponent).position.set(position)
                    }
                })
                row()
                add(TextButton("Torch", skin).apply {
                    addChangeListener {
                        toggleInventory()
                        val torch = gameInitializer.createTorch()
                        val position = Vector3(players.first().get(TransformComponent).position)
                        position.y -= 0.5F
                        torch.get(TransformComponent).position.set(position)
                    }
                })
                row()
                add(Label("Placeables", skin))
                row()
                add(TextButton("Grass Bed", skin).apply {
                    addChangeListener {
                        toggleInventory()
                        placing = Placement(entityCreator.createGrassBed())
                    }
                })
                pack()
            }).apply {
                top()
            }
            pack()
            centerOnScreen()
            makeSolid()
        }
    }

    private fun Stack.addAmountOverlay(itemStack: Entity) {
        val overlay = Table(assets.skin)
        val amount = itemStack.tryGet(StackSizeComponent)?.let { it.amount } ?: 1
        if (amount > 1) {
            overlay.add(Label(amount.toString(), assets.skin).apply {
                setAlignment(Align.right or Align.bottom)
            }).expand().bottom().right().padRight(5F)
            add(overlay)
        }
    }

    fun update(deltaTime: Float) {
        screenTable.update()
        stage.act(deltaTime)
        updatePlacing()
    }

    private fun updatePlacing() {
        placing?.apply {
            val pos = Vector3(Gdx.input.x.toFloat(), Gdx.input.y.toFloat(), 0F)
            worldCam.unproject(pos)
            entity.get(TransformComponent).position.set(pos.snapToWorld())
        }
    }

    fun draw() {
        screenTable.draw()
        stage.draw()
    }
}

class Placement(val entity: Entity) {
    private val originalAlpha = sprite().color.a
    init {
//        entity.get(SpriteComponent).sprite.color = Color.GREEN
        sprite().setAlpha(1/3F)
    }

    fun install() {
        sprite().setAlpha(2/3F)
        entity.add(UnfinishedPlaceableComponent(mapOf("grass_item" to 2)))
        entity.componentRemoved.add { signal, ent ->
            if (!ent.has(UnfinishedPlaceableComponent)) {
                sprite().setAlpha(originalAlpha)
            }
        }
    }

    private fun sprite() = entity.get(SpriteComponent).sprite
}

class CraftingComponent(val type: CraftingComponentType) : Component {
    companion object : ComponentResolver<CraftingComponent>(CraftingComponent::class.java)
}

enum class CraftingComponentType {
    CRAFTING_TABLE
}

class UnfinishedPlaceableComponent(val requiredIngredients: Map<String, Int>) : Component {
    companion object : ComponentResolver<UnfinishedPlaceableComponent>(UnfinishedPlaceableComponent::class.java)
}
