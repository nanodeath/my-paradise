package name.maxaller.myparadise

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.math.Vector2
import com.google.inject.Inject

class CreatureAiBounceSystem @Inject constructor() : IteratingSystem(Family.all(TransformComponent::class.java, PhysicsComponent::class.java, CreatureAiComponent.Bounce::class.java).get()) {
    override fun processEntity(entity: Entity, deltaTime: Float) {
        val bounce = entity.get(CreatureAiComponent.Bounce)
        bounce.timeSinceLastBounce += deltaTime
        if (bounce.timeSinceLastBounce >= bounce.timeBetweenBounces) {
            val angle = calculateAngle(bounce)
            val impulseVector = calculateImpulseVector(angle, bounce)
            val body = entity.physics.body
            body.applyLinearImpulse(impulseVector, body.worldCenter, true)
            bounce.timeSinceLastBounce = 0F
        }
    }

    private fun calculateAngle(bounce: CreatureAiComponent.Bounce) = 90F - bounce.angleOfBounceDegrees + 2 * (Math.random() * bounce.angleOfBounceDegrees)

    private fun calculateImpulseVector(angle: Double, bounce: CreatureAiComponent.Bounce) = Vector2(bounce.forceOfBounce, 0F).rotate(angle.toFloat())
}