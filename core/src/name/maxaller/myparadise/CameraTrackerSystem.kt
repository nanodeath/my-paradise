package name.maxaller.myparadise

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.EntitySystem
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.utils.ImmutableArray
import com.badlogic.gdx.graphics.OrthographicCamera
import com.google.inject.Inject

class CameraTrackerSystem @Inject constructor(private val camera: OrthographicCamera) : EntitySystem() {
    private lateinit var entities: ImmutableArray<Entity>
    override fun addedToEngine(engine: Engine) {
        entities = engine.getEntitiesFor(Family.all(PlayerComponent::class.java).get())
    }

    override fun update(deltaTime: Float) {
        val player = entities.first()
        camera.position.set(player.transform.position)
        camera.update()
    }
}