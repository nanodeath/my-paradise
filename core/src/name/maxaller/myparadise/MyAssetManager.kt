package name.maxaller.myparadise

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.google.inject.Inject
import com.google.inject.Singleton
import com.badlogic.gdx.utils.Array as GdxArray

@Singleton
class MyAssetManager @Inject constructor() {
    lateinit var skin: Skin

    internal val assetManager = AssetManager()

    /*
    What type to create?
    1. If it doesn't animate or rotate, you want a TextureRegion.
    2. If it rotates but doesn't animate, you want a Sprite.
    3. If it animates (may rotate), you want a GdxArray<Sprite>.
    4. You basically never want a Texture -- use TextureRegion instead.
     */

    /*
    Remember to call initSprite() (or initSprites()) when working with sprites! Otherwise you'll get MASSIVE portrayals
    of said sprite.
     */

    private fun creaturesItems(): TextureAtlas {
        return assetManager.get("output/creatures_items.txt", TextureAtlas::class.java)
    }

    fun maleSpriteFaceRight(): Sprite {
        return creaturesItems().createSprite("stand_right").initSprite()
    }

    fun maleSpriteFaceLeft(): Sprite {
        return creaturesItems().createSprite("stand_left").initSprite()
    }

    fun maleSpriteWalkRight(): GdxArray<Sprite> {
        return creaturesItems().createSprites("walk_right").initSprites()
    }

    fun maleSpriteWalkLeft(): GdxArray<Sprite> {
        return creaturesItems().createSprites("walk_left").initSprites()
    }

    private fun terrain(): TextureAtlas {
        return assetManager.get("output/terrain.txt", TextureAtlas::class.java)
    }

    fun dirtInnerTextureRegion(): TextureRegion = terrain().findRegion("jungle_dirt_inner")
    fun dirtTopTextureRegion(): TextureRegion = terrain().findRegion("jungle_dirt_tc")
    fun dirtLeftTopTextureRegion(): TextureRegion = terrain().findRegion("jungle_dirt_tl")
    fun dirtRightTopTextureRegion(): TextureRegion = terrain().findRegion("jungle_dirt_tr")

    fun getSprite(imageFilename: String, atlasKey: String): Sprite {
        val createSprite = assetManager.get(imageFilename, TextureAtlas::class.java).createSprite(atlasKey)
        require(createSprite != null) { "Unable to find sprite $atlasKey within $imageFilename" }
        return createSprite.initSprite()
    }

    fun backgroundTextureRegion(): TextureRegion = TextureRegion(assetManager.get("output/bg_jungle_flat.png", Texture::class.java))

    fun craftingTableTextureRegion(): TextureRegion = creaturesItems().findRegion("crafting_table")

    fun torchSprite(): Sprite = creaturesItems().createSprite("torch").initSprite()

    fun slimeSprites(): GdxArray<out Sprite> = creaturesItems().createSprites("slime").initSprites()
}

class VisualAssetComponent(val visual: GameAssets.AbstractVisual) : Component {
    companion object : ComponentResolver<VisualAssetComponent>(VisualAssetComponent::class.java)
}

class WeaponAssetComponent(val weapon: GameAssets.Weapon) : Component {
    companion object : ComponentResolver<WeaponAssetComponent>(WeaponAssetComponent::class.java)
}

object GameAssets {
    const val TERRAIN_ATLAS = "output/terrain.txt"

    /** NOTE: Assets should not implement this class directly, but rather a subclass */
    interface AbstractVisual {
        fun getTextureRegion(assets: AssetManager): TextureRegion
        fun getSprite(assets: AssetManager): Sprite
    }

    interface Visual : AbstractVisual {
        val imageFilename: String
    }

    interface TextureVisual : Visual {
        val imageSize: ImageSize

        fun getImageSize(texture: Texture): Pair<Int, Int> {
            return if (imageSize == AutoImageSize) {
                Pair(texture.width, texture.height)
            } else {
                imageSize.run {
                    Pair(x, y)
                }
            }
        }

        override fun getTextureRegion(assets: AssetManager): TextureRegion {
            return TextureRegion(assets.get(imageFilename, Texture::class.java))
        }
    }

    interface TextureRegionVisual : TextureVisual {
        val imagePosition: Pair<Int, Int>
        override fun getTextureRegion(assets: AssetManager): TextureRegion {
            val texture = assets.get(imageFilename, Texture::class.java)
            val (width, height) = getImageSize(texture)
            val (x, y) = imagePosition
            return TextureRegion(texture, x, y, width, height)
        }

        override fun getSprite(assets: AssetManager): Sprite {
            return Sprite(getTextureRegion(assets))
        }
    }

    interface AtlasSpriteVisual : Visual {
        val atlasKey: String
        override fun getTextureRegion(assets: AssetManager): TextureRegion {
            return assets.get(imageFilename, TextureAtlas::class.java).findRegion(atlasKey)
        }

        override fun getSprite(assets: AssetManager): Sprite {
            val createSprite = assets.get(imageFilename, TextureAtlas::class.java).createSprite(atlasKey)
            require(createSprite != null) { "Unable to find sprite $atlasKey within $imageFilename" }
            return createSprite.initSprite()
        }
    }

    interface AtlasSpriteSequenceVisual : AtlasSpriteVisual {
        val timePerFrame: Float

        fun getTextureRegions(assets: AssetManager): GdxArray<out TextureRegion> {
            return assets.get(imageFilename, TextureAtlas::class.java).findRegions(atlasKey)
        }
    }

    interface ImageSize {
        val x: Int
        val y: Int
    }

    object AutoImageSize : ImageSize {
        override val x = -1
        override val y = -1

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other?.javaClass != javaClass) return false

            other as AutoImageSize

            if (x != other.x) return false
            if (y != other.y) return false

            return true
        }

        override fun hashCode(): Int {
            var result = x
            result = 31 * result + y
            return result
        }
    }

    interface Equippable {
        val equipOrigin: Pair<Float, Float>
    }

    interface CompoundVisual : AbstractVisual {
        val visuals: Iterable<Pair<String, AbstractVisual>>
    }

    interface Weapon : Equippable {
        val damageType: DamageType
        val damageMultiplierSoftTerrain: Float // e.g. dirt
        val damageMultiplierHardTerrain: Float // e.g. rock
        val physicsFile: String?
        val physicsFileBodyName: String?
    }

    // https://www.reddit.com/r/DnD/comments/3v8btt/dd_5e_damage_type_explanations/
    enum class DamageType {
        Slashing, Bludgeoning, Piercing
    }

    abstract class BaseWeapon() : AtlasSpriteVisual, Weapon {
        override val imageFilename = "output/creatures_items.txt"
    }

    object WoodenClub : BaseWeapon() {
        override val atlasKey = "wooden_club"
        override val equipOrigin = -0.5F to 0F
        override val damageType = DamageType.Bludgeoning
        override val damageMultiplierSoftTerrain = 0.01F
        override val damageMultiplierHardTerrain = 0.05F
        override val physicsFile = null
        override val physicsFileBodyName = null
    }

    object Shovel : BaseWeapon() {
        override val atlasKey = "shovel"
        override val equipOrigin = -0.5F to 0F
        override val damageType = DamageType.Slashing
        override val damageMultiplierSoftTerrain = 1F
        override val damageMultiplierHardTerrain = 0.2F
        override val physicsFile = "output/shovel.xml"
        override val physicsFileBodyName = "shovel"
    }

    object WoodenDoor : AtlasSpriteVisual {
        override val imageFilename = TERRAIN_ATLAS
        override val atlasKey = "wooden_door_closed"
    }

    object WoodenDoorOpen : AtlasSpriteVisual {
        override val imageFilename = TERRAIN_ATLAS
        override val atlasKey = "wooden_door_open"
    }

    object GrassItem : AtlasSpriteVisual {
        override val imageFilename = TERRAIN_ATLAS
        override val atlasKey = "grass"
    }
}

/**
 * Sets the size and origin of the sprite in world units.
 */
internal fun Sprite.initSprite() = apply {
    setSize(width.pixelsToMeters, height.pixelsToMeters)
    setOrigin(width / 2F, height / 2F)
}

/**
 * Calls [initSprite] on all sprites in this thing.
 */
private fun <T : Iterable<Sprite>> T.initSprites() = apply {
    forEach { it: Sprite -> it.initSprite() }
}

fun TextureRegion.toComponent() = TextureRegionComponent(this)
fun Sprite.toComponent() = SpriteComponent(this)
