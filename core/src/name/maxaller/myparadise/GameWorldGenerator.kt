package name.maxaller.myparadise

import com.badlogic.gdx.math.MathUtils
import com.google.inject.Inject
import com.sudoplay.joise.module.*
import java.awt.Color
import java.awt.image.BufferedImage
import java.io.File
import java.util.*
import javax.imageio.ImageIO

class GameWorldGenerator @Inject constructor() {
    fun generate(width: Int, height: Int) : Array<Array<String?>> {
        val widthOfWave = MathUtils.PI2
        val desiredNumberOfWaves = width / 35
        val heightOfWave = 5

        val xWidth = (widthOfWave * desiredNumberOfWaves) / width // sample between 0 and number of waves we want * 2 PI
        val yHeight = 1F / height // equal parts
        val lowestPoint = 15 // lowest point that sees light

        return 0.until(width).map { w ->
            val x = w * xWidth
            0.until(height).map { h ->
                val y = h * yHeight
                val rawHeightAtCurrentX = MathUtils.sin(x)
                val positiveHeightAtCurrentX = rawHeightAtCurrentX + 1 // [-1, -1] -> [0, 2]
                val scaledHeightAtCurrentX = positiveHeightAtCurrentX * 0.5F * (heightOfWave * yHeight) // scale to appropriate height
                val realHeightAtCurrentX = scaledHeightAtCurrentX + lowestPoint * yHeight

                if (realHeightAtCurrentX >= y) {
                    "dirt"
                } else {
                    null
                }
            }.toTypedArray()
        }.toTypedArray()
    }

    // https://github.com/SudoPlayGames/Joise/blob/master/examples/com/sudoplay/joise/examples/Example_02.java
    // http://accidentalnoise.sourceforge.net/minecraftworlds.html
    fun generate2(width: Int, height: Int) : Array<Array<String?>> {
        val random = Random()
        val seed = random.nextLong()

        /*
     * ground_gradient
     */

        // ground_gradient
        val groundGradient = ModuleGradient()
        groundGradient.setGradient(0.0, 0.0, 0.0, 1.0)

        // lowland_shape_fractal
        val lowlandShapeFractal = ModuleFractal(ModuleFractal.FractalType.BILLOW, ModuleBasisFunction.BasisType.GRADIENT, ModuleBasisFunction.InterpolationType.QUINTIC)
        lowlandShapeFractal.setNumOctaves(6)
        lowlandShapeFractal.setFrequency(2.0)
        lowlandShapeFractal.seed = seed

        // lowland_autocorrect
//        val lowlandAutoCorrect = ModuleAutoCorrect(0.0, 1.0)
//        lowlandAutoCorrect.setSource(lowlandShapeFractal)
//        lowlandAutoCorrect.calculate()

        // lowland_scale
        val lowlandScale = ModuleScaleOffset()
        lowlandScale.setScale(0.5)
        lowlandScale.setOffset(0.5)
        lowlandScale.setSource(/*lowlandAutoCorrect*/lowlandShapeFractal)

        // lowland_y_scale
//        val lowlandYScale = ModuleScaleDomain()
//        lowlandYScale.setScaleY(0.0)
//        lowlandYScale.setSource(lowlandScale)

        // lowland_terrain
        val lowlandTerrain = ModuleTranslateDomain()
        lowlandTerrain.setAxisYSource(/*lowlandYScale*/lowlandScale)
        lowlandTerrain.setSource(groundGradient)

//        val lowlandPerturb = ModuleFractal(ModuleFractal.FractalType.FBM, ModuleBasisFunction.BasisType.GRADIENT, ModuleBasisFunction.InterpolationType.QUINTIC)
//        lowlandPerturb.setNumOctaves(6)
//        lowlandPerturb.setFrequency(3.0)
//        lowlandPerturb.seed = seed

        val lowlandSelect = ModuleSelect()
        lowlandSelect.setLowSource(0.0)
        lowlandSelect.setHighSource(1.0)
        lowlandSelect.setControlSource(/*lowlandPerturb*/lowlandTerrain)
        lowlandSelect.setThreshold(0.1)
        lowlandSelect.setFalloff(0.0)

        val image = BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)

        val last = lowlandSelect

        val scale = 1F
        val ret = 0.until(width).map { w ->
//            val x = w * xWidth
            (height - 1).downTo(0).map { h ->
                val w2 = w.toDouble() / width * 10 * scale
                val h2 = h.toDouble() / height
                val v = last[w2, h2]
                println("($w2, $h2) -> $v")
                val r = MathUtils.clamp(v, 0.0, 1.0).toFloat()
                val color = Color(r, r, r)
                image.setRGB(w, h, color.rgb)
                if (v >= 1.0) {
                    "dirt"
                } else {
                    null
                }
            }.toTypedArray()
        }.toTypedArray()

        println("Writing image of size $width x $height")
        ImageIO.write(image, "png", File("terrain.png"))


        return ret

        /*
     * lowland
     */

//        // lowland_shape_fractal
//        val lowlandShapeFractal = ModuleFractal(ModuleFractal.FractalType.BILLOW, ModuleBasisFunction.BasisType.GRADIENT, ModuleBasisFunction.InterpolationType.QUINTIC)
//        lowlandShapeFractal.setNumOctaves(2)
//        lowlandShapeFractal.setFrequency(0.25)
//        lowlandShapeFractal.seed = seed
//
//        // lowland_autocorrect
//        val lowlandAutoCorrect = ModuleAutoCorrect(0.0, 1.0)
//        lowlandAutoCorrect.setSource(lowlandShapeFractal)
//        lowlandAutoCorrect.calculate()
//
//        // lowland_scale
//        val lowlandScale = ModuleScaleOffset()
//        lowlandScale.setScale(0.125)
//        lowlandScale.setOffset(-0.45)
//        lowlandScale.setSource(lowlandAutoCorrect)
//
//        // lowland_y_scale
//        val lowlandYScale = ModuleScaleDomain()
//        lowlandYScale.setScaleY(0.0)
//        lowlandYScale.setSource(lowlandScale)
//
//        // lowland_terrain
//        val lowlandTerrain = ModuleTranslateDomain()
//        lowlandTerrain.setAxisYSource(lowlandYScale)
//        lowlandTerrain.setSource(groundGradient)
//
//        /*
//     * highland
//     */
//
//        // highland_shape_fractal
//        val highlandShapeFractal = ModuleFractal(ModuleFractal.FractalType.FBM, ModuleBasisFunction.BasisType.GRADIENT, ModuleBasisFunction.InterpolationType.QUINTIC)
//        highlandShapeFractal.setNumOctaves(4)
//        highlandShapeFractal.setFrequency(2.0)
//        highlandShapeFractal.seed = seed
//
//        // highland_autocorrect
//        val highlandAutoCorrect = ModuleAutoCorrect(-1.0, 1.0)
//        highlandAutoCorrect.setSource(highlandShapeFractal)
//        highlandAutoCorrect.calculate()
//
//        // highland_scale
//        val highlandScale = ModuleScaleOffset()
//        highlandScale.setScale(0.25)
//        highlandScale.setOffset(0.0)
//        highlandScale.setSource(highlandAutoCorrect)
//
//        // highland_y_scale
//        val highlandYScale = ModuleScaleDomain()
//        highlandYScale.setScaleY(0.0)
//        highlandYScale.setSource(highlandScale)
//
//        // highland_terrain
//        val highlandTerrain = ModuleTranslateDomain()
//        highlandTerrain.setAxisYSource(highlandYScale)
//        highlandTerrain.setSource(groundGradient)
//
//        /*
//     * mountain
//     */
//
//        // mountain_shape_fractal
//        val mountainShapeFractal = ModuleFractal(ModuleFractal.FractalType.RIDGEMULTI, ModuleBasisFunction.BasisType.GRADIENT, ModuleBasisFunction.InterpolationType.QUINTIC)
//        mountainShapeFractal.setNumOctaves(8)
//        mountainShapeFractal.setFrequency(1.0)
//        mountainShapeFractal.seed = seed
//
//        // mountain_autocorrect
//        val mountainAutoCorrect = ModuleAutoCorrect(-1.0, 1.0)
//        mountainAutoCorrect.setSource(mountainShapeFractal)
//        mountainAutoCorrect.calculate()
//
//        // mountain_scale
//        val mountainScale = ModuleScaleOffset()
//        mountainScale.setScale(0.45)
//        mountainScale.setOffset(0.15)
//        mountainScale.setSource(mountainAutoCorrect)
//
//        // mountain_y_scale
//        val mountainYScale = ModuleScaleDomain()
//        mountainYScale.setScaleY(0.1)
//        mountainYScale.setSource(mountainScale)
//
//        // mountain_terrain
//        val mountainTerrain = ModuleTranslateDomain()
//        mountainTerrain.setAxisYSource(mountainYScale)
//        mountainTerrain.setSource(groundGradient)
//
//        /*
//     * terrain
//     */
//
//        // terrain_type_fractal
//        val terrainTypeFractal = ModuleFractal(ModuleFractal.FractalType.FBM, ModuleBasisFunction.BasisType.GRADIENT, ModuleBasisFunction.InterpolationType.QUINTIC)
//        terrainTypeFractal.setNumOctaves(3)
//        terrainTypeFractal.setFrequency(0.125)
//        terrainTypeFractal.seed = seed
//
//        // terrain_autocorrect
//        val terrainAutoCorrect = ModuleAutoCorrect(0.0, 1.0)
//        terrainAutoCorrect.setSource(terrainTypeFractal)
//        terrainAutoCorrect.calculate()
//
//        // terrain_type_y_scale
//        val terrainTypeYScale = ModuleScaleDomain()
//        terrainTypeYScale.setScaleY(0.0)
//        terrainTypeYScale.setSource(terrainAutoCorrect)
//
//        // terrain_type_cache
//        val terrainTypeCache = ModuleCache()
//        terrainTypeCache.setSource(terrainTypeYScale)
//
//        // highland_mountain_select
//        val highlandMountainSelect = ModuleSelect()
//        highlandMountainSelect.setLowSource(highlandTerrain)
//        highlandMountainSelect.setHighSource(mountainTerrain)
//        highlandMountainSelect.setControlSource(terrainTypeCache)
//        highlandMountainSelect.setThreshold(0.65)
//        highlandMountainSelect.setFalloff(0.2)
//
//        // highland_lowland_select
//        val highlandLowlandSelect = ModuleSelect()
//        highlandLowlandSelect.setLowSource(lowlandTerrain)
//        highlandLowlandSelect.setHighSource(highlandMountainSelect)
//        highlandLowlandSelect.setControlSource(terrainTypeCache)
//        highlandLowlandSelect.setThreshold(0.25)
//        highlandLowlandSelect.setFalloff(0.15)
//
//        // highland_lowland_select_cache
//        val highlandLowlandSelectCache = ModuleCache()
//        highlandLowlandSelectCache.setSource(highlandLowlandSelect)
//
//        // ground_select
//        val groundSelect = ModuleSelect()
//        groundSelect.setLowSource(0.0)
//        groundSelect.setHighSource(1.0)
//        groundSelect.setThreshold(0.5)
//        groundSelect.setControlSource(highlandLowlandSelectCache)
//
//        /*
//     * cave
//     */
//
//        // cave_shape
//        val caveShape = ModuleFractal(ModuleFractal.FractalType.RIDGEMULTI, ModuleBasisFunction.BasisType.GRADIENT, ModuleBasisFunction.InterpolationType.QUINTIC)
//        caveShape.setNumOctaves(1)
//        caveShape.setFrequency(8.0)
//        caveShape.seed = seed
//
//        // cave_attenuate_bias
//        val caveAttenuateBias = ModuleBias(0.825)
//        caveAttenuateBias.setSource(highlandLowlandSelectCache)
//
//        // cave_shape_attenuate
//        val caveShapeAttenuate = ModuleCombiner(ModuleCombiner.CombinerType.MULT)
//        caveShapeAttenuate.setSource(0, caveShape)
//        caveShapeAttenuate.setSource(1, caveAttenuateBias)
//
//        // cave_perturb_fractal
//        val cavePerturbFractal = ModuleFractal(ModuleFractal.FractalType.FBM, ModuleBasisFunction.BasisType.GRADIENT, ModuleBasisFunction.InterpolationType.QUINTIC)
//        cavePerturbFractal.setNumOctaves(6)
//        cavePerturbFractal.setFrequency(3.0)
//        cavePerturbFractal.seed = seed
//
//        // cave_perturb_scale
//        val cavePerturbScale = ModuleScaleOffset()
//        cavePerturbScale.setScale(0.25)
//        cavePerturbScale.setOffset(0.0)
//        cavePerturbScale.setSource(cavePerturbFractal)
//
//        // cave_perturb
//        val cavePerturb = ModuleTranslateDomain()
//        cavePerturb.setAxisXSource(cavePerturbScale)
//        cavePerturb.setSource(caveShapeAttenuate)
//
//        // cave_select
//        val caveSelect = ModuleSelect()
//        caveSelect.setLowSource(1.0)
//        caveSelect.setHighSource(0.0)
//        caveSelect.setControlSource(cavePerturb)
//        caveSelect.setThreshold(0.8)
//        caveSelect.setFalloff(0.0)
//
//        /*
//     * final
//     */
//
//        // ground_cave_multiply
//        val groundCaveMultiply = ModuleCombiner(ModuleCombiner.CombinerType.MULT)
//        groundCaveMultiply.setSource(0, caveSelect)
//        groundCaveMultiply.setSource(1, groundSelect)

    }
}