package name.maxaller.myparadise

import com.badlogic.ashley.core.Family
import com.badlogic.ashley.systems.IntervalIteratingSystem
import com.badlogic.gdx.utils.Array as GdxArray

/**
 * Not sure if we want to keep this around or not. It's much simpler and more efficient, but still a lot of open
 * questions. Increment or multiple by the large prime? What happens when entities are added and deleted by this system?
 */
abstract class IntervalIteratingSystemSkipping(family: Family, interval: Float, priority: Int, private val bucketCount: Int) : IntervalIteratingSystem(family, interval, priority) {
    private companion object {
        const val largePrime = 105943
    }
    private var idx = 0

    override fun updateInterval() {
        val entitiesToProcess = entities.size() / bucketCount
        for (i in 1..entitiesToProcess) {
            idx %= entities.size()
            processEntity(entities[idx])
            // Should this be plus or multiply?
            idx += largePrime
        }
    }
}