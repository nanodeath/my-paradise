right.png
size: 314, 51
format: RGBA8888
filter: Linear,Linear
repeat: none
stand_right
  rotate: false
  xy: 141, 1
  size: 31, 49
  orig: 46, 54
  offset: 5, 0
  index: -1
walk_right
  rotate: false
  xy: 211, 1
  size: 34, 48
  orig: 39, 51
  offset: 2, 0
  index: 1
walk_right
  rotate: false
  xy: 247, 1
  size: 32, 48
  orig: 37, 50
  offset: 2, 1
  index: 2
walk_right
  rotate: false
  xy: 281, 1
  size: 32, 48
  orig: 38, 52
  offset: 0, 1
  index: 3
walk_right
  rotate: false
  xy: 37, 1
  size: 33, 49
  orig: 40, 52
  offset: 1, 0
  index: 4
walk_right
  rotate: false
  xy: 174, 1
  size: 35, 48
  orig: 40, 51
  offset: 0, 0
  index: 5
walk_right
  rotate: false
  xy: 1, 1
  size: 34, 49
  orig: 40, 52
  offset: 1, 0
  index: 6
walk_right
  rotate: false
  xy: 72, 1
  size: 33, 49
  orig: 38, 52
  offset: 1, 0
  index: 7
walk_right
  rotate: false
  xy: 107, 1
  size: 32, 49
  orig: 48, 54
  offset: 5, 0
  index: 8
