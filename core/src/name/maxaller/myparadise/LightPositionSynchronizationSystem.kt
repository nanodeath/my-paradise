package name.maxaller.myparadise

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.systems.IteratingSystem
import com.google.inject.Inject

/**
 * Synchronizes lights within [LightComponent]s that are attached to an entity with a [TransformComponent] to the
 * position of that [TransformComponent]. Doesn't handle directionality yet.
 */
class LightPositionSynchronizationSystem @Inject constructor() : IteratingSystem(Family.all(LightComponent::class.java, TransformComponent::class.java).get()) {
    override fun processEntity(entity: Entity, deltaTime: Float) {
        val position = entity.transform.position
        val lightComponent = entity.get(LightComponent)
        lightComponent.light.setPosition(position.x + lightComponent.xOffset, position.y + lightComponent.yOffset)
    }
}