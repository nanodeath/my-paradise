package name.maxaller.myparadise

import box2dLight.DirectionalLight
import box2dLight.Light
import com.badlogic.ashley.core.Component
import com.badlogic.ashley.core.ComponentMapper
import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Animation
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.Joint
import com.badlogic.gdx.physics.box2d.World
import java.util.*
import com.badlogic.gdx.utils.Array as GdxArray

open class ComponentResolver<T : Component>(componentClass: Class<T>) {
    val MAPPER: ComponentMapper<T> = ComponentMapper.getFor(componentClass)
    operator fun get(entity: Entity): T = MAPPER.get(entity)
}

class TransformComponent(val position: Vector3, var angleRadians: Float, var scale: Float) : Component {
    constructor(position: Vector2) : this(Vector3(position.x, position.y, 0F))
    constructor(position: Vector2, angleRadians: Float, scale: Float) : this(Vector3(position.x, position.y, 0F), angleRadians, scale)
    constructor(position: Vector3) : this(position, 0F, 1F)
    constructor() : this(Vector3(), 0F, 1F)

    companion object : ComponentResolver<TransformComponent>(TransformComponent::class.java)
}

val Entity.transform: TransformComponent
    get() = get(TransformComponent)

class TextureComponent(val texture: Texture) : Component {
    companion object : ComponentResolver<TextureComponent>(TextureComponent::class.java)
}

val Entity.texture: TextureComponent
    get() = TextureComponent[this]

class TextureRegionComponent(val textureRegion: TextureRegion) : Component {
    var reversed = false
    fun reverse() {
        reversed = !reversed
    }

    companion object : ComponentResolver<TextureRegionComponent>(TextureRegionComponent::class.java)
}

val Entity.textureRegion: TextureRegionComponent
    get() = TextureRegionComponent[this]

class SpriteComponent(val sprite: Sprite) : Component {
    companion object : ComponentResolver<SpriteComponent>(SpriteComponent::class.java)

    var reversed = false
    fun reverse() {
        reversed = !reversed
    }
}

class SpritesComponent(val sprites: GdxArray<Sprite>) : Component {
    companion object : ComponentResolver<SpritesComponent>(SpritesComponent::class.java)
}

val Entity.sprite: SpriteComponent
    get() = SpriteComponent[this]

class PhysicsComponent(val body: Body) : Component {
    private val _additionalBodies = ArrayList<Body>()
    val additionalBodies: List<Body>
        get() = _additionalBodies

    fun manageBody(body: Body) {
        _additionalBodies.add(body)
    }

    fun unmanageBody(body: Body) {
        _additionalBodies.remove(body)
    }

    private val _joints = ArrayList<Joint>()
    val joints: List<Joint>
        get() = _joints

    fun manageJoint(joint: Joint) {
        _joints.add(joint)
    }

    fun destroyAllManagedJoints(world: World) {
        _joints.forEach { world.destroyJoint(it) }
        _joints.clear()
    }

    fun unmanageJoint(joint: Joint) {
        _joints.remove(joint)
    }

    companion object : ComponentResolver<PhysicsComponent>(PhysicsComponent::class.java)
}

val Entity.physics: PhysicsComponent
    get() = PhysicsComponent[this]


class PlayerComponent() : Component {
    var moving = false
    var facingRight = true
    var timeMoving = 0F
    val isOnGround: Boolean
        get() = groundCollisions > 0
    var groundCollisions = 0

    companion object : ComponentResolver<PlayerComponent>(PlayerComponent::class.java)

    var equippedItem: Entity? = null
}

class CharacterAnimationComponent(val faceRight: TextureRegion, val moveRight: GdxArray<out TextureRegion>,
                                  val faceLeft: TextureRegion, val moveLeft: GdxArray<out TextureRegion>) : Component {
    val moveRightAnimation = Animation(0.125F, moveRight, Animation.PlayMode.LOOP)
    val moveLeftAnimation = Animation(0.125F, moveLeft, Animation.PlayMode.LOOP)

    companion object : ComponentResolver<CharacterAnimationComponent>(CharacterAnimationComponent::class.java)
}

class AnimationComponent(frameDuration: Float, images: GdxArray<out TextureRegion>, mode: Animation.PlayMode = Animation.PlayMode.LOOP) : Component {
    val animation = Animation(frameDuration, images, mode)

    companion object : ComponentResolver<AnimationComponent>(AnimationComponent::class.java)
}

class UsesParallaxCamera(val parallaxXSpeed: Float, val parallaxYSpeed: Float) : Component {
    companion object : ComponentResolver<UsesParallaxCamera>(UsesParallaxCamera::class.java)
}

//class TerrainComponent(val terrainType: String, val x: Int, val y: Int, val background: Boolean = false) : Component {
class TerrainComponent(val instance: TerrainInstance, val x: Int, val y: Int, val background: Boolean = false) : Component {
    companion object : ComponentResolver<TerrainComponent>(TerrainComponent::class.java)
}

class TerrainWorldDescription(val dimensions: Vector2, val offset: Vector2, val size: Float) : Component {
    companion object : ComponentResolver<TerrainWorldDescription>(TerrainWorldDescription::class.java)
}

class InventoryComponent() : Component {
    val items: MutableList<Entity> = ArrayList()
    var modificationCount = 0
        private set

    fun count(itemType: ItemType) = items.filter { it.tryGet(ItemTypeComponent)?.itemType == itemType }.map { it.tryGet(StackSizeComponent)?.amount ?: 1 }.sum()
    fun count(terrain: Terrain) = items.filter { it.tryGet(BlockComponent)?.terrain == terrain }.map { it.tryGet(StackSizeComponent)?.amount ?: 1 }.sum()

    fun give(itemType: ItemType, amount: Int = 1) {
        modificationCount++
        items.add(Entity().apply {
            add(VisualAssetComponent(itemType.visualAsset))
            add(ItemTypeComponent(itemType))
            if (amount != 1) {
                add(StackSizeComponent(amount))
            }
        })
    }

    fun give(entity: Entity, amount: Int = 1) {
        modificationCount++
        if (entity.has(BlockComponent)) {
            val terrain = entity.get(BlockComponent).terrain
            val stacks = items.filter { it.tryGet(BlockComponent)?.terrain == terrain }
            if (stacks.isEmpty()) {
                items.add(entity)
                entity.add(StackSizeComponent(amount))
                entity.add(VisualAssetComponent(terrain.item.asVisual()))
            } else {
                stacks.first().get(StackSizeComponent).amount++
            }
        } else {
            items.add(entity)
        }
    }

    fun giveOrStack(itemType: ItemType, amount: Int) {
        val existingStack = items.find { it.tryGet(ItemTypeComponent)?.itemType == itemType }
        if (existingStack != null) {
            modificationCount++
            existingStack.getOrAdd(StackSizeComponent) { StackSizeComponent(0) }.apply {
                this.amount += amount
            }
        } else {
            give(itemType, amount)
        }
    }

    fun take(terrain: Terrain, amount: Int = 1): Boolean {
        val stacks = items.filter { it.tryGet(BlockComponent)?.terrain == terrain && it.get(StackSizeComponent).amount >= amount }
        val stack = stacks.firstOrNull()
        return if (stack != null) {
            stack.get(StackSizeComponent).amount -= amount
            if (stack.get(StackSizeComponent).amount <= 0) {
                items.remove(stack)
            }
            true
        } else {
            false
        }
    }

    fun take(itemType: ItemType, amount: Int = 1): Boolean {
        val item = items.find { it.tryGet(ItemTypeComponent)?.itemType == itemType && (!it.has(StackSizeComponent) || it.get(StackSizeComponent).amount >= amount) }
        return if (item != null) {
            val stack = item.tryGet(StackSizeComponent)
            if (stack != null) {
                stack.amount -= amount
                if (stack.amount <= 0)
                    items.remove(item)
                true
            } else if (amount == 1) {
                items.remove(item)
                true
            } else false
        } else false

    }

    companion object : ComponentResolver<InventoryComponent>(InventoryComponent::class.java)
}

class StackSizeComponent(var amount: Int) : Component {
    companion object : ComponentResolver<StackSizeComponent>(StackSizeComponent::class.java)
}

class ItemTypeComponent(val itemType: ItemType) : Component {
    companion object : ComponentResolver<ItemTypeComponent>(ItemTypeComponent::class.java)
}

class SunComponent(initialDirection: Float, private val offset: Float, private val directionalLight: DirectionalLight) : Component {
    var direction: Float = 0F
        get() = field
        set(value) {
            field = value
            directionalLight.setDirection(value + offset)
        }

    init {
        direction = initialDirection
    }

    companion object : ComponentResolver<SunComponent>(SunComponent::class.java)
}

class MoonComponent(initialDirection: Float, val directionalLight: DirectionalLight) : Component {
    var direction: Float = 0F
        get() = field
        set(value) {
            field = value
            directionalLight.setDirection(value)
        }

    init {
        direction = initialDirection
    }

    companion object : ComponentResolver<MoonComponent>(MoonComponent::class.java)
}

class StarsComponent(initialDirection: Float, val directionalLight: DirectionalLight) : Component {
    var direction: Float = 0F
        get() = field
        set(value) {
            field = value
            directionalLight.setDirection(value)
        }

    init {
        direction = initialDirection
    }

    companion object : ComponentResolver<StarsComponent>(StarsComponent::class.java)
}

class LightComponent(val light: Light, var xOffset: Float, var yOffset: Float) : Component {
    constructor(light: Light) : this(light, 0F, 0F)

    companion object : ComponentResolver<LightComponent>(LightComponent::class.java)
}

class CollidingWithComponent() : Component {
    private val _collisions: MutableSet<Entity> = HashSet()

    val collisions: Collection<Entity>
        get() = _collisions

    fun addCollision(entity: Entity) {
        _collisions.add(entity)
    }

    fun removeCollision(entity: Entity) {
        _collisions.remove(entity)
    }

    val colliding: Boolean
        get() = collisions.isNotEmpty()

    companion object : ComponentResolver<CollidingWithComponent>(CollidingWithComponent::class.java)
}

object CreatureAiComponent {
    class Bounce() : Component {
        var timeSinceLastBounce = 0F
        val timeBetweenBounces = 3F
        val forceOfBounce = 10F
        val angleOfBounceDegrees = 45F

        companion object : ComponentResolver<Bounce>(Bounce::class.java)
    }
}

class HitpointsComponent(val maximum: Int, var current: Int) : Component {
    constructor(maximum: Int) : this(maximum, maximum)

    companion object : ComponentResolver<HitpointsComponent>(HitpointsComponent::class.java)
}

class TrainCarsComponent(val trainSlices: List<Entity>) : Component {
    companion object : ComponentResolver<TrainCarsComponent>(TrainCarsComponent::class.java)
}

class WeaponComponent(val weaponDescription: WeaponDescription) : Component {
    companion object : ComponentResolver<WeaponComponent>(WeaponComponent::class.java)
}

data class WeaponDescription(val weaponType: WeaponType, val baseWeapon: GameAssets.BaseWeapon, val damage: Float)
enum class WeaponType {
    WOODEN_CLUB, SHOVEL
}
