package name.maxaller.myparadise;

import com.badlogic.ashley.core.*;
import com.badlogic.ashley.utils.ImmutableArray;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

// https://github.com/libgdx/ashley/issues/244
public class EntityListenerFamilyBug {
    @Test
    public void succeeds() {
        helper(null);
    }

    @Test
    public void alsoSucceeds() {
        helper(Component2.class);
    }

    @Test
    public void fails() {
        helper(Component1.class);
    }

    @Test
    public void alsoFails() {
        helper(Component3.class);
    }

    private void helper(final Class<? extends Component> componentToRemove) {
        Engine engine = new Engine();
        Entity entity = new Entity();
        entity.add(new Component1());
        entity.add(new Component2());
        entity.add(new Component3());
        engine.addEntity(entity);

        engine.addEntityListener(Family.all(Component1.class).get(), new EntityListener() {
            @Override
            public void entityAdded(Entity entity) { }

            @Override
            public void entityRemoved(Entity entity) {
                if (componentToRemove != null) {
                    entity.remove(componentToRemove);
                }
            }
        });

        ImmutableArray<Entity> entities = engine.getEntitiesFor(Family.all(Component2.class).get());
        assertTrue(entities.contains(entity, true));
        engine.removeEntity(entity);
        // If the test fails, it's always because the following assertion fails.
        assertFalse(entities.contains(entity, true));
    }

    private static class Component1 implements Component {
    }
    private static class Component2 implements Component {
    }
    private static class Component3 implements Component {
    }
}
