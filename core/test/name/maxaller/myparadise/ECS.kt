package name.maxaller.myparadise

import com.badlogic.gdx.utils.ObjectIntMap
import org.junit.Assert
import org.junit.Test
import java.lang.ref.WeakReference
import java.util.*

class MyFamily() {
    internal val mustHave = BitSet()
    internal val mustNotHave = BitSet()
    internal val mustHaveOne = BitSet()
    private val _entities = LinkedHashSet<MyEntity>()
    val entities: Set<MyEntity> = Collections.unmodifiableSet(_entities)

    internal fun offer(entity: MyEntity) {
        if (matches(entity)) {
            add(entity)
        }
    }

    internal fun remove(entity: MyEntity) {
        _entities.remove(entity)
    }

    internal fun refresh(entity: MyEntity) {
        val currentlyContains = _entities.contains(entity)
        val shouldContain = matches(entity)
        if (currentlyContains != shouldContain) {
            if (shouldContain) {
                // if we don't have it, but should
                add(entity)
            } else {
                // if we have it, but shouldn't
                remove(entity)
            }
        }
    }

    private fun add(entity: MyEntity) {
        _entities.add(entity)
    }

    private fun matches(entity: MyEntity): Boolean {
        val entityBits = entity.componentBits
        if (validateMustHave(entityBits)) return false
        if (validateMustNotHave(entityBits)) return false
        return validateMustHaveOne(entityBits)
    }

    private fun validateMustHave(entityBits: BitSet): Boolean {
        var idx = mustHave.nextSetBit(0)
        while (idx >= 0) {
            if (!entityBits.get(idx)) {
                return true
            }
            idx = mustHave.nextSetBit(idx + 1)
        }
        return false
    }

    private fun validateMustNotHave(entityBits: BitSet): Boolean {
        var idx = mustNotHave.nextSetBit(0)
        while (idx >= 0) {
            if (entityBits.get(idx)) {
                return true
            }
            idx = mustHave.nextSetBit(idx + 1)
        }
        return false
    }

    private fun validateMustHaveOne(entityBits: BitSet) = if (mustHaveOne.isEmpty) true else mustHaveOne.intersects(entityBits)

}

class MyEngine() {
    private val families = ArrayList<WeakReference<MyFamily>>()
    private val _entities = LinkedHashSet<MyEntity>()
    val entities: Set<MyEntity> = Collections.unmodifiableSet(_entities)
    private val componentBitMap = ObjectIntMap<Class<out MyComponent>>()
    private var componentCounter = 0

    internal fun getBitFor(componentClass: Class<out MyComponent>): Int {
        if (!componentBitMap.containsKey(componentClass)) {
            componentBitMap.put(componentClass, componentCounter++)
        }
        return componentBitMap[componentClass, -1]
    }

    fun createFamily(myFamilyBuilder: MyFamilyBuilder): MyFamily {
        val family = MyFamily()
        myFamilyBuilder.mustHave.forEach { comp ->
            family.mustHave.set(getBitFor(comp))
        }
        myFamilyBuilder.mustNotHave.forEach { comp ->
            family.mustNotHave.set(getBitFor(comp))
        }
        myFamilyBuilder.mustHaveOne.forEach { comp ->
            family.mustHaveOne.set(getBitFor(comp))
        }
        registerFamily(family)
        return family
    }

    fun addEntity(entity: MyEntity): MyEntity {
        forEachFamily() { it.offer(entity) }
        _entities.add(entity)
        entity.engine = this
        return entity
    }

    fun removeEntity(entity: MyEntity) {
        forEachFamily() { it.remove(entity) }
        entity.engine = null
        _entities.remove(entity)
    }

    internal fun refreshEntity(entity: MyEntity) {
        forEachFamily { it.refresh(entity) }
    }

    private inline fun forEachFamily(cb: (MyFamily) -> Unit) {
        families.fastForEach { family ->
            cb(family)
        }
    }

    private fun registerFamily(myFamily: MyFamily) {
        families.add(WeakReference(myFamily))
        _entities.forEach { myFamily.offer(it) }
    }
}

class MyFamilyBuilder {
    internal val mustHave: MutableList<Class<out MyComponent>> = ArrayList()
    internal val mustNotHave: MutableList<Class<out MyComponent>> = ArrayList()
    internal val mustHaveOne: MutableList<Class<out MyComponent>> = ArrayList()

    fun all(vararg componentClass: Class<out MyComponent>) = apply {
        mustHave.fastAddAll(componentClass)
    }

    fun exclude(vararg componentClass: Class<out MyComponent>) = apply {
        mustNotHave.fastAddAll(componentClass)
    }

    fun one(vararg componentClass: Class<out MyComponent>) = apply {
        mustHaveOne.fastAddAll(componentClass)
    }

    private fun <T> MutableList<T>.fastAddAll(array: Array<out T>): Boolean {
        return when (array.size) {
            0 -> { false }
            1 -> { add(array[0]) }
            2 -> { add(array[0]) or add(array[1]) }
            3 -> { add(array[0]) or add(array[1]) or add(array[2]) }
            4 -> { add(array[0]) or add(array[1]) or add(array[2]) or add(array[3]) }
            5 -> { add(array[0]) or add(array[1]) or add(array[2]) or add(array[3]) or add(array[4]) }
            else -> addAll(array)
        }
    }
}

class MyEntity() { // TODO make constructor internal?
    internal var engine: MyEngine? = null
    internal val componentBits: BitSet = BitSet()

    fun addComponent(comp: MyComponent) {
        addComponentInternal(comp)
        engine?.refreshEntity(this)
    }

    fun addComponents(vararg comp: MyComponent) {
        comp.forEach { addComponentInternal(it) }
        engine?.refreshEntity(this)
    }

    private fun addComponentInternal(comp: MyComponent) {
        val bit = engine?.getBitFor(comp.javaClass) ?: throw IllegalStateException("must add entity to engine first")
        componentBits.set(bit)
    }
}

///////////////////////////////

class ECS {
    @Test
    fun hi() {
        val engine = MyEngine()
        for (i in 1..9999) {

            val entity = engine.addEntity(MyEntity()).apply {
                addComponents(
                        MyComponentInstance(),
                        MyComponentInstance2()
                )
            }
        }
        val entity = engine.addEntity(MyEntity()).apply {
            addComponents(
                    MyComponentInstance(),
                    MyComponentInstance2()
            )
        }
        val family = engine.createFamily(MyFamilyBuilder().all(MyComponentInstance::class.java))
        println(engine.entities.size)
        println(family.entities.size)
//        Assert.assertTrue(family.entities.isEmpty())
        Assert.assertEquals(entity, family.entities.last())
    }
}

class MyComponentInstance : MyComponent {
}

class MyComponentInstance2 : MyComponent {
}

interface MyComponent {
}
