package name.maxaller.myparadise

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.google.inject.Inject

class TerrainTickerSystem @Inject constructor(): IntervalIteratingSystemBuckets(Family.all(TerrainComponent::class.java).get(), 30F, 0, 100) {
    override fun processEntity(entity: Entity) {
        // TODO
    }
}