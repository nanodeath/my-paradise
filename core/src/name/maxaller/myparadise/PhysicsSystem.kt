package name.maxaller.myparadise

import com.badlogic.ashley.core.EntitySystem
import com.badlogic.gdx.physics.box2d.World
import com.google.inject.Inject

class PhysicsSystem @Inject constructor(private val world: World) : EntitySystem() {
    private var accumulator = 0f
    override fun update(deltaTime: Float) {
        // https://github.com/libgdx/libgdx/wiki/Box2d#stepping-the-simulation
        // http://gafferongames.com/game-physics/fix-your-timestep/
        val frameTime = Math.min(deltaTime, 0.25F)
        accumulator += frameTime
        while (accumulator >= TIME_STEP) {
            world.step(TIME_STEP, VELOCITY_ITERATIONS, POSITION_ITERATIONS)
            accumulator -= TIME_STEP
        }
        // Fixed time step
        //world.step(TIME_STEP, VELOCITY_ITERATIONS, POSITION_ITERATIONS)
    }

    companion object {
        private val TIME_STEP = 1.0f / 300f
        private val VELOCITY_ITERATIONS = 6
        private val POSITION_ITERATIONS = 2

        const val CATEGORY_SCENERY = (1 shl 0).toShort()
        const val CATEGORY_CREATURE = (1 shl 1).toShort()
        const val CATEGORY_PLAYER = (1 shl 2).toShort()
        const val CATEGORY_SCENERY_BG = (1 shl 3).toShort()
        const val CATEGORY_HITTABLE = (1 shl 4).toShort()
        const val CATEGORY_ITEM = (1 shl 5).toShort()
        val MASK_CREATURE = CATEGORY_SCENERY or CATEGORY_CREATURE or CATEGORY_PLAYER
        val MASK_PLAYER = CATEGORY_SCENERY or CATEGORY_CREATURE or CATEGORY_ITEM
        val MASK_LIGHTING = CATEGORY_SCENERY
        val MASK_PLAYER_WEAPON = 0.toShort()
        val MASK_PLAYER_WEAPON_SWINGING = CATEGORY_CREATURE or CATEGORY_HITTABLE

        private infix fun Short.or(other: Short): Short = toInt().or(other.toInt()).toShort()
    }
}
