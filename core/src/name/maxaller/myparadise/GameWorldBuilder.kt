package name.maxaller.myparadise

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.World
import com.google.inject.Inject
import com.google.inject.Singleton
import ktx.collections.gdxArrayOf
import java.util.*

@Singleton
class GameWorldBuilder @Inject constructor(private val world: World,
                                           private val engine: Engine,
                                           private val assets: MyAssetManager,
                                           private val terrainTypes: TerrainTypes,
                                           private val entityCreator: EntityCreator,
                                           private val terrainBuilder: TerrainBuilder) {
    fun build(terrain: Array<Array<String?>>, offset: Vector2, size: Float) {
        val position = Vector2()
        engine.addEntity(Entity().apply {
            val dimensions = Vector2(terrain.size.toFloat(), terrain[0].size.toFloat())
            add(TerrainWorldDescription(dimensions, offset, size))
        })

        terrain.forEachIndexed { x, column ->
            column.forEachIndexed { y, terrainType ->
                if (terrainType != null) {
                    position.set(x * size + offset.x, y * size + offset.y)
                    val terrain1 = terrainTypes[terrainType]
                    terrainBuilder.createTerrain(terrain1, position.x.toInt(), position.y.toInt()).apply {
                        physics.body.setTransform(position, 0F)
                    }
                }
            }
        }
        val body = gdxArrayOf<Body>()
        world.getBodies(body)
        body.forEach { body ->
            val entity = body.userData as? Entity
            if (entity != null) {
                val terrainComponent = entity.tryGet(TerrainComponent)
                if (terrainComponent != null) {
                    updateTerrain(entity)
                }
            }
        }
    }

    fun updateTerrain(entity: Entity) {
        val nearbyBodies = LinkedHashSet<Entity>()
        val entityTerrain = entity.get(TerrainComponent)
        val body = entity.physics.body.position
        world.QueryAABB({ fixture ->
            val fixtureEntity = fixture.body.userData as? Entity
            if (fixtureEntity != null) {
                nearbyBodies.add(fixtureEntity)
            }
            true
        }, body.x - 1F, body.y - 1F, body.x + 1F, body.y + 1F)
        val isBg = entityTerrain.background
        val nearbyTerrain = nearbyBodies.filter { it.has(TerrainComponent) && it.get(TerrainComponent).background == isBg }
        var above: Entity? = null
        var left: Entity? = null
        var right: Entity? = null
        var below: Entity? = null
        nearbyTerrain.forEach { n ->
            val neighborTerrain = n.get(TerrainComponent)
            if (neighborTerrain.y == entityTerrain.y + 1 && neighborTerrain.x == entityTerrain.x) above = n
            else if (neighborTerrain.y == entityTerrain.y) { // middle
                if (neighborTerrain.x == entityTerrain.x - 1) left = n
                else if (neighborTerrain.x == entityTerrain.x + 1) right = n
            } else if (neighborTerrain.y == entityTerrain.y - 1 && neighborTerrain.x == entityTerrain.x) below = n
        }
        val terrain = entityTerrain.instance.terrain
        val subtypes = if (!entityTerrain.background || terrain.background == null) terrain.subtypes else terrain.background!!
        val neighborCount = sequenceOf(above, left, right, below).filterNotNull().count()
        val subtype = when (neighborCount) {
            1 -> {
                if (above != null) subtypes.verticalSolo
                else if (left != null) subtypes.horizontalSolo
                else if (right != null) subtypes.horizontalSolo
                else /* below */ subtypes.verticalSolo
            }
            2 -> {
                if (left != null && right != null) subtypes.horizontalSolo
                else if (above != null && below != null) subtypes.verticalSolo
                else if (left != null  && above != null) subtypes.bottomRight
                else if (right != null && above != null) subtypes.bottomLeft
                else if (left != null  && below != null) subtypes.topRight
                else if (right != null && below != null) subtypes.topLeft
                else throw AssertionError()
            }
            3 -> {
                if (left == null) subtypes.left
                else if (above == null) subtypes.topCenter
                else if (right == null) subtypes.right
                else if (below == null) subtypes.bottomCenter
                else throw AssertionError()
            }
            4 -> subtypes.center
            else -> return
        }
        if (subtype == subtypes.topCenter) {
            if (Math.random() < 0.2) {
                entityCreator.createGrass(entityTerrain.x, entityTerrain.y + 1)
            }
        }
        entity.add(TerrainComponent(entityTerrain.instance.copy(terrainSubtype = subtype), entityTerrain.x, entityTerrain.y, entityTerrain.background))
        entity.updateSprite()
    }

    private fun Entity.updateSprite() {
        val terrainInstance = get(TerrainComponent).instance
        val createSprite = assets.assetManager.get(terrainInstance.terrain.subtypes.sprite, TextureAtlas::class.java).createSprite(terrainInstance.terrainSubtype.image)
        requireNotNull(createSprite)
        createSprite.initSprite()
        add(createSprite.toComponent())

        physics.body.setAngle(terrainInstance.terrainSubtype.rotation.toFloat())
    }
}
