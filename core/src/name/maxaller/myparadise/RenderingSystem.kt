package name.maxaller.myparadise

import box2dLight.RayHandler
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.systems.SortedIteratingSystem
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.utils.TimeUtils
import com.google.inject.Inject
import java.util.*

class RenderingSystem @Inject constructor(private val batch: SpriteBatch,
                                          private val camera: OrthographicCamera,
                                          @GuiCam private val guiCam: OrthographicCamera,
                                          private val rayHandler: RayHandler,
                                          private val assets: MyAssetManager,
                                          private val lighting: Lighting) :
        SortedIteratingSystem(Family.all(TransformComponent::class.java).one(TextureComponent::class.java, TextureRegionComponent::class.java, SpriteComponent::class.java, CharacterAnimationComponent::class.java, AnimationComponent::class.java).get(),
                Comparator<Entity> { o1, o2 -> o1.transform.position.z.compareTo(o2.transform.position.z) }) {

    private val startOfGame = TimeUtils.millis()
    override fun update(deltaTime: Float) {
        initializeDefaultCamera()
        batch.begin()
        super.update(deltaTime)
        batch.end()
        lighting.forceStaticLightUpdateIfNecessary()
        rayHandler.setCombinedMatrix(camera)
        rayHandler.updateAndRender()
    }

    private fun initializeDefaultCamera() {
        batch.projectionMatrix = camera.combined
    }

    override fun processEntity(entity: Entity, deltaTime: Float) {
        val restoreDefaultCamera = if (entity.has(UsesParallaxCamera)) {
            val px = entity.get(UsesParallaxCamera)
            batch.projectionMatrix = camera.parallax(px.parallaxXSpeed, px.parallaxYSpeed)
            true
        } else {
            false
        }

        if (entity.has(UsesParallaxCamera)) {
            val px = entity.get(UsesParallaxCamera)
            batch.projectionMatrix = camera.parallax(px.parallaxXSpeed, px.parallaxYSpeed)
        }

        val position = entity.transform.position
        entity.tryGet(TextureComponent)?.let { textureComponent ->
            renderTexture(position, textureComponent.texture)
        }

        entity.tryGet(TextureRegionComponent)?.let { textureRegionComponent ->
            renderTextureRegion(entity, position, textureRegionComponent.textureRegion, textureRegionComponent.reversed)
        }

        entity.tryGet(SpriteComponent)?.let { spriteComponent ->
            renderSprite(entity, position, spriteComponent.sprite, spriteComponent.reversed)
        }

        entity.tryGet(CharacterAnimationComponent)?.let { animationComponent ->
            entity.tryGet(PlayerComponent)?.let { playerComponent ->
                val texture = if (playerComponent.facingRight && playerComponent.moving) {
                    animationComponent.moveRightAnimation.getKeyFrame(playerComponent.timeMoving)
                } else if (playerComponent.moving) {
                    animationComponent.moveLeftAnimation.getKeyFrame(playerComponent.timeMoving)
                } else if (playerComponent.facingRight) {
                    animationComponent.faceRight
                } else {
                    animationComponent.faceLeft
                }
                renderTextureRegion(entity, position, texture)
            }
        }

        entity.tryGet(AnimationComponent)?.let { animationComponent ->
            val timeSinceStart = TimeUtils.timeSinceMillis(startOfGame) / 1000F
            val textureRegion = animationComponent.animation.getKeyFrame(timeSinceStart)
            renderTextureRegion(entity, position, textureRegion)
        }

        if (restoreDefaultCamera) {
            initializeDefaultCamera()
        }
    }

    private fun renderTexture(position: Vector3, texture: Texture) {
        batch.draw(texture,
                position.x - texture.width.pixelsToMeters / 2F, position.y - texture.height.pixelsToMeters / 2F,
                texture.width.pixelsToMeters, texture.height.pixelsToMeters)
    }

    private fun renderTextureRegion(entity: Entity, position: Vector3, textureRegion: TextureRegion, reversed: Boolean = false) {
        if (textureRegion is Sprite) {
            return renderSprite(entity, position, textureRegion, reversed)
        }
        val img = textureRegion
        val width = if (reversed) {
            -img.regionWidth.pixelsToMeters
        } else {
            img.regionWidth.pixelsToMeters
        }
        val height = img.regionHeight.pixelsToMeters
        val scale = entity.transform.scale

        batch.draw(img,
                position.x - width / 2, position.y - height / 2,
                width / 2F, height / 2F,
                width, height,
                scale, scale,
                entity.transform.angleRadians.toDegrees
        )
    }

    private fun renderSprite(entity: Entity, position: Vector3, sprite: Sprite, reversed: Boolean = false) {
        sprite.apply {
            val desiredRotationDeg = entity.transform.angleRadians.toDegrees
            if (desiredRotationDeg != rotation)
                rotation = desiredRotationDeg
            setFlip(false, reversed)
            setPosition(position.x - sprite.width / 2F, position.y - sprite.height / 2F)
            setScale(entity.transform.scale)
        }

        sprite.draw(batch)
    }
}