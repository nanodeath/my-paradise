package name.maxaller.myparadise

import box2dLight.RayHandler
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.google.inject.Inject

class DisposalClass @Inject constructor(
        private val spriteBatch: SpriteBatch,
        private val rayHandler: RayHandler
) {
    fun dispose() {
        spriteBatch.dispose()
        rayHandler.dispose()
    }
}