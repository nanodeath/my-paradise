package name.maxaller.myparadise

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.EntitySystem
import com.google.inject.Inject

class HudSystem @Inject constructor(private val hud: Hud) : EntitySystem() {
    override fun addedToEngine(engine: Engine) {
        hud.setEngine(engine)
    }

    override fun update(deltaTime: Float) {
        hud.update(deltaTime)
        hud.draw()
    }
}