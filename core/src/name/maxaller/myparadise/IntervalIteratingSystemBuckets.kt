    package name.maxaller.myparadise

    import com.badlogic.ashley.core.Engine
    import com.badlogic.ashley.core.Entity
    import com.badlogic.ashley.core.EntityListener
    import com.badlogic.ashley.core.Family
    import com.badlogic.ashley.systems.IntervalIteratingSystem
    import com.badlogic.gdx.utils.Array as GdxArray

    /**
     * Partition all applicable entities into [bucketCount] separate "buckets", and then process these incrementally over
     * the full timespan of [interval].
     *
     * How to determine what to set [bucketCount] to? Each bucket should probably not contain more than, oh, around 100
     * elements. So take your expected number of entities, divide by 100, and that's approximately how much buckets you
     * should have.
     *
     * Considerations:
     * 1. This implementation is somewhat memory intensive -- it scales linearly with the number of entities in the provided
     *    family.
     * 2. Deletions (i.e. removing entities from the engine) are moderately expensive -- it has to scan through an array of
     *    average size entities/[bucketCount] to find the entity before removing it.
     * 3. Entities are stored unordered.
     * 4. Every time entities are processed, *approximately* entities/[bucketCount] are processed each time. Only
     *    approximately, though.
     * 5. Each entity is guaranteed to be processed exactly once every [interval] seconds.
     * 6. A reasonable implementation of #hashcode() is assumed for the Entities.
     *
     * Ex. If you have 10,000 entities, an interval of 10 seconds, and a bucket count of 100, then every 100 ms 100 entities
     * will be processed. (One bucket every 10/100 ms, where each bucket contains roughly 100 entities).
     *
     * @param interval the interval (in seconds) during which all entities will have been processed once.
     * @param priority The priority to execute this system with (lower means higher priority). Default 0.
     * @param bucketCount Number of internal buckets to partition all entities into.
     */
    abstract class IntervalIteratingSystemBuckets(family: Family, interval: Float, priority: Int, private val bucketCount: Int) :
            IntervalIteratingSystem(family, interval / bucketCount, priority) {
        private val buckets = Array<GdxArray<Entity>>(bucketCount, { GdxArray(false, 8) })
        private val entityListener = MyEntityListener()
        private var currentBucketIdx = 0

        override fun addedToEngine(engine: Engine) {
            super.addedToEngine(engine)
            engine.addEntityListener(family, entityListener)
        }

        override fun removedFromEngine(engine: Engine) {
            super.removedFromEngine(engine)
            engine.removeEntityListener(entityListener)
        }

        override fun updateInterval() {
            buckets[currentBucketIdx].forEach { processEntity(it) }

            incrementBucketCount()
        }

        private fun incrementBucketCount() {
            currentBucketIdx++
            if (currentBucketIdx >= buckets.size) {
                currentBucketIdx = 0
            }
        }

        private inner class MyEntityListener : EntityListener {
            override fun entityAdded(entity: Entity) {
                buckets[entity.bucketIdx].add(entity)
            }

            override fun entityRemoved(entity: Entity) {
                // Moderately expensive; this is why you should favor more buckets
                buckets[entity.bucketIdx].removeValue(entity, true)
            }

            private val Entity.bucketIdx: Int
                get() = hashCode() % bucketCount
        }
    }