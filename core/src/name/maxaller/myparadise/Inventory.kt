package name.maxaller.myparadise

import com.badlogic.ashley.core.Component
import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.BodyDef
import com.badlogic.gdx.physics.box2d.Filter
import com.badlogic.gdx.physics.box2d.PolygonShape
import com.badlogic.gdx.physics.box2d.World
import com.google.common.collect.HashBiMap
import com.google.inject.Inject
import com.google.inject.Singleton

class EntityInInventory(val entity: Entity) : Component {
    companion object : ComponentResolver<EntityInInventory>(EntityInInventory::class.java)
}

interface ItemType : HasName {
    val visualAsset: GameAssets.AbstractVisual
}

class TerrainBuilder @Inject constructor(private val engine: Engine, private val world: World, private val assets: MyAssetManager) {
    fun createTerrain(terrain: Terrain, x: Int, y: Int, background: Boolean = false): Entity {
        val size = 1F
        val shape = PolygonShape().apply {
            setAsBox(size / 2F, size / 2F)
        }
        val bodyDef = BodyDef().apply {
            type = BodyDef.BodyType.StaticBody
        }
        val filter = Filter().apply {
            categoryBits = if (!background) PhysicsSystem.CATEGORY_SCENERY else PhysicsSystem.CATEGORY_SCENERY_BG
        }

        val body = world.createBody(bodyDef).apply {
            setTransform(Vector2(x.toFloat(), y.toFloat()), 0F)
            createFixture(shape, 0F).apply {
                friction = 5F
                filterData = filter
            }
        }

        val initialSubtype = if (background) terrain.background!!.center else terrain.subtypes.center

        val entity = Entity().apply {
            body.userData = this
            val terrainInstance = TerrainInstance(terrain, initialSubtype)
            add(TransformComponent().apply {
                position.z = if (!background) 0F else -10F
            })
            add(PhysicsComponent(body).apply {
                body.setAngle(terrainInstance.terrainSubtype.rotation.toFloat())
            })
            add(TerrainComponent(terrainInstance, x, y, background = background))

            val createSprite = assets.assetManager.get(terrainInstance.terrain.subtypes.sprite, TextureAtlas::class.java).createSprite(terrainInstance.terrainSubtype.image)
            createSprite.initSprite()
            add(createSprite.toComponent())

            add(HitpointsComponent(terrain.vitals.health))
        }
        engine.addEntity(entity)
        return entity
    }
}

data class TerrainInstance(val terrain: Terrain, val terrainSubtype: TerrainSubtype)

class BlockBuilder @Inject constructor(private val terrainTypes: TerrainTypes, private val engine: Engine, private val world: World, private val assets: MyAssetManager) {
    fun createItem(name: String) = createItem(terrainTypes[name])

    fun createItem(terrain: Terrain): Entity {
        val entity = Entity().apply {
            add(TransformComponent())
            add(BlockComponent(terrain))
            val sprite = assets.getSprite(terrain.item.sprite, terrain.item.image)
            add(sprite.toComponent())
        }
        return entity
    }

    fun createDetachedBlock(terrain: Terrain): Entity {
        val size = 0.5F
        val shape = PolygonShape().apply {
            setAsBox(size / 2F, size / 2F)
        }
        val bodyDef = BodyDef().apply {
            type = BodyDef.BodyType.DynamicBody
        }
        val filter = Filter().apply {
            categoryBits = PhysicsSystem.CATEGORY_PLAYER
        }

        val body = world.createBody(bodyDef).apply {
            setTransform(position, 0F)
            createFixture(shape, 0F).apply {
                friction = 5F
                filterData = filter
                isSensor = true
            }
        }

        val entity = Entity().apply {
            add(TransformComponent().apply { scale = 0.5F })
            add(PhysicsComponent(body))
            add(BlockComponent(terrain))
            val sprite = assets.getSprite(terrain.item.sprite, terrain.item.image)
            add(sprite.toComponent())
            body.userData = this
        }
        return entity
    }

    fun createPlaceable(placeableItemType: PlaceableItemType, x: Int, y: Int): Entity {
        val size = 1F
        val shape = PolygonShape().apply {
            setAsBox(size / 2F, size / 2F)
        }
        val bodyDef = BodyDef().apply {
            type = BodyDef.BodyType.StaticBody
        }
        val filter = Filter().apply {
            categoryBits = PhysicsSystem.CATEGORY_SCENERY
        }

        val body = world.createBody(bodyDef).apply {
            setTransform(Vector2(x.toFloat(), y.toFloat()), 0F)
            createFixture(shape, 0F).apply {
                friction = 5F
                filterData = filter
            }
        }

        val entity = Entity().apply {
            add(TransformComponent())
            add(PhysicsComponent(body))
            add(PlaceableComponent(placeableItemType))

            val sprite = placeableItemType.visualAsset.getSprite(assets.assetManager)
            add(sprite.toComponent())
            body.userData = this
        }
        engine.addEntity(entity)
        return entity
    }

    fun createDetachedPlaceable(placeableItemType: PlaceableItemType): Entity {
        val size = 0.5F
        val shape = PolygonShape().apply {
            setAsBox(size / 2F, size / 2F)
        }
        val bodyDef = BodyDef().apply {
            type = BodyDef.BodyType.DynamicBody
        }
        val filter = Filter().apply {
            categoryBits = PhysicsSystem.CATEGORY_PLAYER
        }

        val body = world.createBody(bodyDef).apply {
            setTransform(position, 0F)
            createFixture(shape, 0F).apply {
                friction = 5F
                filterData = filter
                isSensor = true
            }
        }

        val entity = Entity().apply {
            add(TransformComponent().apply { scale = 0.5F })
            add(PhysicsComponent(body))
            add(PlaceableComponent(placeableItemType))

            add(placeableItemType.visualAsset.getTextureRegion(assets.assetManager).toComponent())
            body.userData = this
        }
        return entity
    }
}

class BlockComponent(val terrain: Terrain) : Component {
    companion object : ComponentResolver<BlockComponent>(BlockComponent::class.java)
}

class PlaceableComponent(val placeableItemType: PlaceableItemType) : Component {
    companion object : ComponentResolver<PlaceableComponent>(PlaceableComponent::class.java)
}

@Singleton
class TerrainTypes @Inject constructor() {
    private val terrainTypes = HashBiMap.create<String, Terrain>()
    fun registerTerrainType(name: String, terrain: Terrain) {
        require(!terrainTypes.containsKey(name)) { "Terrain type $name already registered" }
        terrainTypes.put(name, terrain)
    }

    operator fun get(name: String) = terrainTypes[name]!!
    fun tryGet(name: String): Terrain? = terrainTypes[name]
}

interface InteractiveItemType {
    val interactive: Boolean
}

abstract class PlaceableItemType : ItemType, HasName

object InteractionRequestedComponent : Component, ComponentResolver<InteractionRequestedComponent>(InteractionRequestedComponent::class.java)

class LinkedInteractionComponent(val other: Entity) : Component {
    companion object : ComponentResolver<LinkedInteractionComponent>(LinkedInteractionComponent::class.java)
}

abstract class SmallItemType : ItemType, HasName

object Items {
    object WoodenDoor : PlaceableItemType(), InteractiveItemType {
        override val visualAsset = GameAssets.WoodenDoor
        override val name = "wooden_door_closed"
        override val interactive = true
    }

    object WoodenDoorOpen : PlaceableItemType(), InteractiveItemType {
        override val visualAsset = GameAssets.WoodenDoorOpen
        override val name = "wooden_door_open"
        override val interactive = true
    }

    object GrassItem : SmallItemType() {
        override val visualAsset = GameAssets.GrassItem
        override val name = "grass_item"
    }
}

interface HasName {
    val name: String
}
