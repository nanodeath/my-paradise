package name.maxaller.myparadise

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.graphics.g2d.TextureRegion

/**
 * Not sure how I feel about this class. But the data needs to live somewhere, and I don't feel like directly on the
 * entities themselves is the best place. We can eventually read all this in from a file, though.
 */
object ItemOracle {
    @Deprecated("boo")
    fun getTextureRegionFor(entity: Entity, assets: MyAssetManager): TextureRegion {
        entity.tryGet(EntityInInventory)?.let { it.entity }?.let { innerEntity ->
            return innerEntity.get(VisualAssetComponent).visual.getSprite(assets.assetManager)
        }

        return entity.get(VisualAssetComponent).visual.getSprite(assets.assetManager)
    }
}