package name.maxaller.myparadise

import com.badlogic.ashley.core.Component
import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.Fixture
import com.badlogic.gdx.physics.box2d.World
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.utils.Array
import java.lang.ref.WeakReference
import java.util.*

val Int.pixelsToMeters: Float
    get() = this / 32F

val Float.pixelsToMeters: Float
    get() = this / 32F

fun <T : Component> Entity.tryGet(componentResolver: ComponentResolver<T>): T? {
    return componentResolver.MAPPER.get(this)
}

fun <T : Component> Entity.get(componentResolver: ComponentResolver<T>): T {
    return tryGet(componentResolver) ?: throw IllegalStateException("This entity doesn't even HAVE a ${componentResolver.javaClass.name}!")
}

fun <T : Component> Entity.has(componentResolver: ComponentResolver<T>): Boolean {
    return componentResolver.MAPPER.has(this)
}

inline fun <T : Component> Entity.getOrAdd(componentResolver: ComponentResolver<T>, creator: () -> T): T {
    // Return it if we already have it
    tryGet(componentResolver)?.let { return it }
    // Otherwise generate it, add it, and return it
    return creator().apply { add(this) }
}

inline fun <T : Component> Entity.addUnlessPresent(componentResolver: ComponentResolver<T>, creator: () -> T) {
    if (!has(componentResolver)) {
        add(creator())
    }
}

@Suppress("UNUSED_PARAMETER")
inline fun <reified T : Component> Entity.remove(componentResolver: ComponentResolver<T>) {
    remove(T::class.java)
}

val Float.toDegrees: Float
    get() = MathUtils.radiansToDegrees * this
val Float.toRadians: Float
    get() = MathUtils.degreesToRadians * this

fun Vector3.to2d() = Vector2(x, y)

/**
 * Copies the x and y coordinates from the given [position].
 */
fun Vector3.set(position: Vector2): Vector3 {
    x = position.x
    y = position.y
    return this
}

/**
 * Modifies in place!
 */
fun Vector2.flipX(): Vector2 {
    x = -x
    return this
}

fun World.removeAllBodies() {
    val bodies = Array<Body>().apply { getBodies(this) }
    bodies.forEach { body ->
        destroyBody(body)
    }
}

fun Actor.centerOnScreen(x: Boolean = true, y: Boolean = true) {
    if (x && y) {
        setPosition((Gdx.graphics.width - width) / 2F, (Gdx.graphics.height - height) / 2F)
    } else if (x) {
        this.x = (Gdx.graphics.width - width) / 2F
    } else if (y) {
        this.y = (Gdx.graphics.height - height) / 2F
    }
}

fun Actor.makeSolid() {
    touchable = Touchable.enabled
    addListener(object : ClickListener() {
        override fun clicked(event: InputEvent, x: Float, y: Float) {
            event.cancel()
        }
    })
}

fun <T> Actor.findUserObject(userObjectClass: Class<T>): T? {
    var actor: Actor? = this
    while (actor != null) {
        val obj = actor.userObject
        if (obj != null) {
            @Suppress("UNCHECKED_CAST")
            if (userObjectClass.isAssignableFrom(obj.javaClass)) {
                return obj as T
            }
        }
        actor = actor.parent
    }
    return null
}

fun <T : Actor> T.addChangeListener(function: T.() -> Unit) {
    addListener(object : ChangeListener() {
        override fun changed(event: ChangeEvent, actor: Actor) {
            apply(function)
        }
    })
}

fun Engine.containsEntity(entity: Entity): Boolean {
    return entities.contains(entity, true)
}

/**
 * Degrees.
 */
fun Body.setAngle(degrees: Float) {
    setAngleRad(degrees.toRadians)
}

fun Body.setAngleRad(radians: Float) {
    setTransform(position, radians)
}

fun Body.setPosition(targetPosition: Vector2) {
    setTransform(targetPosition, angle)
}

fun Body.setPosition(x: Float, y: Float) {
    setTransform(x, y, angle)
}

fun Entity.dropToGround(distanceAboveGround: Float = 1F) {
    val body = physics.body
    val world = body.world
    world.raycast(body.position, Vector2(0F, -1F)) { fixture, point, normal -> RaycastResult.CLOSEST }?.let { fixture ->
        physics.body.setPosition(Vector2(fixture.body.position).add(0F, distanceAboveGround))
    }
}

fun World.raycast(start: Vector2, direction: Vector2, distance: Float = 100F, cb: (fixture: Fixture, point: Vector2, normal: Vector2) -> RaycastResult): Fixture? {
    var lastFixture: Fixture? = null
    rayCast({ fixture, point, normal, fraction ->
        lastFixture = fixture
        when (cb(fixture, point, normal)) {
            RaycastResult.FILTER -> -1F
            RaycastResult.TERMINATE -> 0F
            RaycastResult.CLOSEST -> fraction
            RaycastResult.CONTINUE -> 1F
        }
    }, start, Vector2(direction).scl(distance).add(start))
    return lastFixture
}

enum class RaycastResult {
    FILTER, TERMINATE, CLOSEST, CONTINUE
}

/**
 * Find and remove the given value, while assuming the list is unordered.
 */
fun <T> unorderedRemove(value: T, list: MutableList<T>): Boolean {
    return if (list is RandomAccess) {
        val idx = list.indexOf(value)
        return if (idx >= 0) {
            list.unorderedRemoveAt(idx)
            true
        } else {
            false
        }
    } else {
        list.remove(value)
    }
}

fun <T> MutableList<T>.unorderedRemoveAt(idx: Int): T {
    val lastIdx = lastIndex
    if (idx < 0 || idx > lastIdx) {
        throw ArrayIndexOutOfBoundsException(idx)
    }
    return if (idx == lastIdx) {
        removeAt(idx)
    } else {
        val current = this[idx]
        this[idx] = removeAt(lastIdx)
        current
    }
}


/**
 * Automatically removes expired [WeakReference]s. Reversed. Assumes list is unordered.
 */
inline fun <T : WeakReference<E>, E, F> F.fastForEach(cb: (E) -> Unit)
        where F : MutableList<T>//, F : RandomAccess // TODO bug https://youtrack.jetbrains.com/issue/KT-14048
{
    fastForEachIndexed { t, i ->
        val e: E? = t.get()
        if (e == null) {
            unorderedRemoveAt(i)
        } else {
            cb(e)
        }
    }
}

/**
 * Reversed, doesn't use iterator.
 */
inline fun <T> List<T>.fastForEachIndexed(cb: (T, Int) -> Unit) {
    for (i in lastIndex downTo 0) {
        cb(this[i], i)
    }
}

/**
 * Reversed, doesn't use iterator.
 */
inline fun <T> List<T>.fastForEachIndexed(cb: (T) -> Unit) {
    for (i in lastIndex downTo 0) {
        cb(this[i])
    }
}

fun Fixture.setFilterBits(maskBits: Short? = null, categoryBits: Short? = null, groupIndex: Short? = null) {
    val filter = filterData
    if (maskBits != null) filter.maskBits = maskBits
    if (categoryBits != null) filter.categoryBits = categoryBits
    if (groupIndex != null) filter.groupIndex = groupIndex
    filterData = filter
}

fun Vector2.snapToWorld() = apply {
    set(Math.round(x).toFloat(), Math.round(y).toFloat())
}

fun Vector3.snapToWorld() = apply {
    set(Math.round(x).toFloat(), Math.round(y).toFloat(), z)
}
