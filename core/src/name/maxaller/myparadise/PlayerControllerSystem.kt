package name.maxaller.myparadise

import com.badlogic.ashley.core.Component
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.InputAdapter
import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.CircleShape
import com.badlogic.gdx.physics.box2d.PolygonShape
import com.badlogic.gdx.physics.box2d.World
import com.badlogic.gdx.physics.box2d.joints.RevoluteJoint
import com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef
import com.badlogic.gdx.physics.box2d.joints.WeldJoint
import com.badlogic.gdx.physics.box2d.joints.WeldJointDef
import com.badlogic.gdx.utils.TimeUtils
import com.google.inject.Inject
import com.google.inject.Singleton

@Singleton
class PlayerControllerSystem @Inject constructor(
        private val camera: OrthographicCamera,
        private val world: World,
        private val hud: Hud,
        private val blockBuilder: BlockBuilder,
        private val terrainTypes: TerrainTypes,
        private val terrainBuilder: TerrainBuilder,
        private val gameWorldBuilder: GameWorldBuilder,
        private val equipper: ItemEquipper
) : IteratingSystem(Family.all(PlayerComponent::class.java).get()) {
    private var moveRight = false
    private var moveLeft = false
    private var jump = false
    private var sprinting = false
    var saveRequested = false
    var loadRequested = false
    var swingingWeapon = false
    private val clickedAt: Vector2 = Vector2()
    private var rotate: Float = 0F
    private var alternateAction: Boolean = false

    init {
        Gdx.input.inputProcessor = InputMultiplexer(hud.stage, hud, MyInputAdapter())
    }

    companion object {
        private const val moveSpeed = 150000F // force
        private val jumpImpulse = moveSpeed / 500F / 2F // impulse
        private const val baseMaxSpeed = 4F
    }

    override fun processEntity(entity: Entity, deltaTime: Float) {
        val body = entity.physics.body
        val horizontalVelocity = body.linearVelocity.x // remember, + is right
        val maximumVelocity = if (sprinting) baseMaxSpeed * 2 else baseMaxSpeed
        if (moveLeft) {
            if (horizontalVelocity > -maximumVelocity) {
                body.applyForceToCenter(-moveSpeed * deltaTime, 0F, true)
            }
        }
        if (moveRight) {
            if (horizontalVelocity < maximumVelocity) {
                body.applyForceToCenter(moveSpeed * deltaTime, 0F, true)
            }
        }
        entity.tryGet(PlayerComponent)?.apply {
            if (swingingWeapon) {
                swingingWeapon = false
                val interactiveEntity = findClickedInteractiveEntity()
                if (interactiveEntity != null) {
                    interactiveEntity.add(InteractionRequestedComponent)
                } else {
                    val equipped = equippedItem
                    if (equipped != null) {
                        if (equipped.has(WeaponComponent)) {
                            entity.addUnlessPresent(SwingingWeaponComponent) { SwingingWeaponComponent() }
                        } else if (equipped.has(BlockComponent)) {
                            val terrain = equipped.get(BlockComponent).terrain
                            val position = Vector2(clickedAt).snapToWorld()

                            if (!alternateAction) {
                                // Place in foreground
                                tryPlaceBlock(terrain, entity, position)
                            } else if (terrain.background != null) {
                                // Place in background
                                tryPlaceBlock(terrain, entity, position, background = true)
                            }
                        } else if (equipped.has(PlaceableComponent)) {
                            val placeableItemType = equipped.get(PlaceableComponent).placeableItemType
                            val position = Vector2(clickedAt).snapToWorld()
                            tryPlacePlaceable(placeableItemType, entity, position)
                        }
                    }
                }
            }
            entity.tryGet(SwingingWeaponComponent)?.apply {
                val timeSinceStart = TimeUtils.timeSinceMillis(this.startedSwingAt)
                val alpha = timeSinceStart.toFloat() / SwingingWeaponComponent.durationOfSwing

                val tippingPoint = 0.8F
                val joint = entity.physics.joints[0] as RevoluteJoint
                joint.enableMotor(true)
                joint.maxMotorTorque = 50F
                val highestAngle = 90F + 45F
                if (alpha < tippingPoint) {
                    equippedItem?.physics?.body?.fixtureList?.forEach { it.setFilterBits(maskBits = PhysicsSystem.MASK_PLAYER_WEAPON) }
                    // raising weapon
                    joint.motorSpeed = (highestAngle / tippingPoint).toRadians
                } else if (alpha <= 1F) {
                    equippedItem?.physics?.body?.fixtureList?.forEach { it.setFilterBits(maskBits = PhysicsSystem.MASK_PLAYER_WEAPON_SWINGING) }
                    // swinging weapon
                    joint.maxMotorTorque = 200F
                    joint.motorSpeed = -(highestAngle / (1 - tippingPoint)).toRadians * 2
                } else {
                    equippedItem?.physics?.body?.fixtureList?.forEach { it.setFilterBits(maskBits = PhysicsSystem.MASK_PLAYER_WEAPON) }
                    // done
                    joint.motorSpeed = 0F
                    joint.maxMotorTorque = 0.005F
                    entity.remove(SwingingWeaponComponent)
                }
                if (!entity.get(PlayerComponent).facingRight) {
                    joint.motorSpeed *= -1
                }
            }
        }
        entity.tryGet(PlayerComponent)?.apply {
            moving = moveLeft or moveRight
            if (moving) {
                if (facingRight != moveRight) {
                    flipX(entity.physics.body)
                    equippedItem?.let {
                        flipX(it.physics.body)
                        it.tryGet(SpriteComponent)?.reverse()
                    }
                }
                facingRight = moveRight

                timeMoving += deltaTime
                if (sprinting) {
                    timeMoving += deltaTime
                }
            } else {
                timeMoving = 0F
                if (isOnGround) {
                    if (Math.abs(horizontalVelocity) > 0.1F) {
                        body.setLinearVelocity(horizontalVelocity * 0.9F, body.linearVelocity.y)
                    } else if (!MathUtils.isZero(horizontalVelocity)) {
                        body.setLinearVelocity(0F, body.linearVelocity.y)
                    }
                }
            }
            if (jump && isOnGround) {
                body.applyLinearImpulse(0F, jumpImpulse, body.worldCenter.x, body.worldCenter.y, true)
            }
        }
    }

    private fun findClickedInteractiveEntity(): Entity? = getEntityAtPoint(clickedAt) { entity ->
        val interactiveItemType = entity.tryGet(PlaceableComponent)?.placeableItemType as? InteractiveItemType
        interactiveItemType != null && interactiveItemType.interactive
    }

    private fun tryPlaceBlock(terrain: Terrain, entity: Entity, position: Vector2, background: Boolean = false) {
        if (getBlockAtPoint(clickedAt) == null && entity.get(InventoryComponent).take(terrain)) {
            terrainBuilder.createTerrain(terrain, position.x.toInt(), position.y.toInt(), background = background).apply {
                physics.body.setAngle(rotate)
            }
            if (entity.get(InventoryComponent).count(terrain) <= 0) {
                equipper.unequipWeapon(entity)
            }
            val x = position.x.toInt()
            val y = position.y.toInt()
            updateTerrain(x, y)
            updateTerrain(x + 1, y)
            updateTerrain(x - 1, y)
            updateTerrain(x, y + 1)
            updateTerrain(x, y - 1)
        }
    }

    private fun tryPlacePlaceable(itemType: PlaceableItemType, entity: Entity, position: Vector2) {
        val aboveClickedAt = Vector2(clickedAt).add(0F, 1F)
        if (getBlockAtPoint(clickedAt) == null && getBlockAtPoint(aboveClickedAt) == null && entity.get(InventoryComponent).take(itemType)) {
            val first = blockBuilder.createPlaceable(itemType, position.x.toInt(), position.y.toInt())
            if (itemType == Items.WoodenDoor) {
                // place additional door piece
                val second = blockBuilder.createPlaceable(itemType, position.x.toInt(), position.y.toInt() + 1)
                first.add(LinkedInteractionComponent(second))
                second.add(LinkedInteractionComponent(first))
            }
            if (entity.get(InventoryComponent).count(itemType) <= 0) {
                equipper.unequipWeapon(entity)
            }
        }
    }

    private fun updateTerrain(x: Int, y: Int) {
        val block = getBlockAtPoint(x, y) ?: return
        if (block.has(TerrainComponent)) {
            gameWorldBuilder.updateTerrain(block)
        }
    }

    private fun getBlockAtPoint(x: Int, y: Int, background: Boolean = false) = getBlockAtPoint(Vector2(x.toFloat(), y.toFloat()), background)

    private fun getBlockAtPoint(worldPosition: Vector2, background: Boolean = false): Entity? =
            getEntityAtPoint(worldPosition) { entity ->
                entity.has(TerrainComponent) && entity.get(TerrainComponent).background == background
            }

    private fun getEntityAtPoint(worldPosition: Vector2, predicate: (Entity) -> Boolean): Entity? {
        var theEntity: Entity? = null
        world.QueryAABB({ fixture ->
            val entity = fixture.body.userData as? Entity
            if (entity != null && fixture.testPoint(worldPosition) && predicate(entity)) {
                theEntity = entity
                false
            } else {
                true
            }
        }, worldPosition.x, worldPosition.y, worldPosition.x, worldPosition.y)
        return theEntity
    }

    private fun flipX(body: Body) {
        val vec = Vector2()
        body.fixtureList.forEach { fixture ->
            val shape = fixture.shape
            when (shape) {
                is CircleShape -> {
                    shape.position = Vector2(-shape.position.x, shape.position.y)
                }
                is PolygonShape -> {
                    val newVector = FloatArray(shape.vertexCount * 2) { 0F }
                    for (i in 0..(shape.vertexCount - 1)) {
                        shape.getVertex(i, vec)
                        val idx = i * 2
                        newVector[idx] = -vec.x // flip
                        newVector[idx + 1] = vec.y // unchanged
                    }
                    shape.set(newVector)
                }
            }
        }
        body.jointList.map { it.joint }.forEach { joint ->
            val newJointDef = when (joint) {
                is RevoluteJoint -> {
                    RevoluteJointDef().apply {
                        bodyA = joint.bodyA
                        bodyB = joint.bodyB
                        enableMotor = joint.isMotorEnabled
                        enableLimit = joint.isLimitEnabled
                        localAnchorA.set(joint.localAnchorA).flipX()
                        localAnchorB.set(joint.localAnchorB).flipX()
                        // TODO why the hell does this work
                        lowerAngle = -joint.upperLimit
                        upperAngle = -joint.lowerLimit
                        motorSpeed = -joint.motorSpeed
                        // TODO also need to just update the angle directly, I think, otherwise it "jumps"
                        referenceAngle = flipAngleAcrossYAxis(joint.referenceAngle)
                        maxMotorTorque = joint.maxMotorTorque
                    }
                }
                is WeldJoint -> {
                    WeldJointDef().apply {
                        bodyA = joint.bodyA
                        bodyB = joint.bodyB
                        localAnchorA.set(joint.localAnchorA)
                        localAnchorB.set(joint.localAnchorB).flipX() // weapon
                        // TODO update after https://github.com/libgdx/libgdx/issues/4330 is implemented
                        referenceAngle = bodyB.angle - bodyA.angle
                    }
                }
                else -> {
                    System.err.println("Warning: joint type ${joint.type} not handled")
                    null
                }
            }
            if (newJointDef != null) {
                world.destroyJoint(joint)

                world.createJoint(newJointDef)
            }
        }
    }

    private fun flipAngleAcrossYAxis(angleRadians: Float) = Vector2(1F, 0F).rotateRad(angleRadians).flipX().angleRad()

    private inner class MyInputAdapter : InputAdapter() {
        override fun keyDown(keycode: Int): Boolean {
            when (keycode) {
                Input.Keys.RIGHT, Input.Keys.D -> moveRight = true
                Input.Keys.LEFT, Input.Keys.A -> moveLeft = true
                Input.Keys.SPACE, Input.Keys.UP, Input.Keys.W -> jump = true
                Input.Keys.SHIFT_LEFT, Input.Keys.SHIFT_RIGHT -> {
                    sprinting = true
                    alternateAction = true
                }
                Input.Keys.S -> {
                    if (Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT)) {
                        saveRequested = true
                    }
                }
                Input.Keys.L -> {
                    if (Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT)) {
                        loadRequested = true
                    }
                }
                Input.Keys.I -> {
                    hud.toggleInventory()
                }
                Input.Keys.ESCAPE -> {
                    Gdx.app.exit()
                }
            }

            return true
        }

        override fun keyUp(keycode: Int): Boolean {
            when (keycode) {
                Input.Keys.RIGHT, Input.Keys.D -> moveRight = false
                Input.Keys.LEFT, Input.Keys.A -> moveLeft = false
                Input.Keys.SPACE, Input.Keys.UP, Input.Keys.W -> jump = false
                Input.Keys.SHIFT_LEFT, Input.Keys.SHIFT_RIGHT -> {
                    sprinting = false
                    alternateAction = false
                }
            }

            return true
        }

        override fun keyTyped(character: Char): Boolean {
            when (character) {
                'r' -> rotate += 90
            }
            return true
        }

        override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
            if (button == Input.Buttons.LEFT) {

                val clickedAtWorld = camera.unproject(Vector3(screenX.toFloat(), screenY.toFloat(), 0F)).to2d()
//                world.QueryAABB({ fixture ->
//                    val entity = fixture.body.userData as? Entity
//                    if (fixture.testPoint(clickedAtWorld) && entity != null && !entity.has(PlayerComponent)) {
//                        engine.removeEntity(entity)
//                        false
//                    } else {
//                        true
//                    }
//                }, clickedAtWorld.x, clickedAtWorld.y, clickedAtWorld.x, clickedAtWorld.y)
                swingingWeapon = true
                clickedAt.set(clickedAtWorld)
            } else if (button == Input.Buttons.RIGHT) {
                hud.toggleContextMenu()
            }

            return true
        }
    }
}

class SwingingWeaponComponent() : Component {
    val startedSwingAt = TimeUtils.millis()

    companion object : ComponentResolver<SwingingWeaponComponent>(SwingingWeaponComponent::class.java) {
        val timeBetweenSwings = 1000F
        val durationOfSwing = 1000F
    }
}
