# Data Model: 10/14/2016

I've managed to put off thinking about this for this long, but no longer! It's time to come up with a proper data model
for items, placeables, and terrain.

## Terrain

Terrain has the following properties:

Name
Image (atlas + key)
Drops
Origin
Hardness
Density
Additional Components

But, this isn't quite right actually. Even just the ground has various visual permutations: top left, center, top right, inner, bottom left, bottom, bottom right (though these last 4 might be the same).
Eventually, grass and other plants might grow on top. More importantly, when the player digs up a top-left soil block, it
should appear only as "soil" in their inventory, not a specific visual position of soil.

So perhaps it's more like this, more complicated thing:

Terrain:
name: string
visuals: VisualTerrain
physics: PhysicalTerrain
attributes: VitalsTerrain
item: ItemTerrain

VisualTerrain:
topLeftImage: atlas.key (string.string or &other, for a reference)
topCenterImage: atlas.key
topRightImage: atlas.key
leftImage: atlas.key
centerImage: atlas.key
rightImage: atlas.key
bottomLeftImage: atlas.key
bottomCenterImage: atlas.key
bottomRightImage: atlas.key

PhysicalTerrain:
density: float

VitalsTerrain:
health: int
hardness: int
vulnerabilities: string list

ItemTerrain:
canPickUp: Boolean
maximumStackSize: Int
image: atlas.key
weight: Float