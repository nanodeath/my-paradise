package name.maxaller.myparadise

import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.utils.BufferUtils
import com.google.inject.Guice
import com.google.inject.Injector

class MyParadiseGame : Game() {
    private lateinit var disposal: DisposalClass

    private lateinit var injector: Injector

    override fun create() {
        println("Max texture size: ${getMaxTextureSize()}×${getMaxTextureSize()}")

        injector = Guice.createInjector(MainModule())
        disposal = injector.getInstance(DisposalClass::class.java)
        setScreen(injector.getInstance(LoadingScreen::class.java))
    }

    /**
     * Maximum size of any texture, as a square, in pixels. On my laptop it's 16384.
     *
     * http://stackoverflow.com/a/35637138/341772
     */
    private fun getMaxTextureSize(): Int {
        val buffer = BufferUtils.newIntBuffer(16)
        Gdx.gl.glGetIntegerv(GL20.GL_MAX_TEXTURE_SIZE, buffer)
        return buffer.get(0)
    }

    override fun render() {
        super.render()
        screen.let { sc ->
            if (sc is LoadingScreen && sc.doneLoading) {
                setScreen(injector.getInstance(GameScreen::class.java))
            }
        }
    }

    override fun dispose() {
        super.dispose()
        disposal.dispose()
    }
}
