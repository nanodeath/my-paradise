package name.maxaller.myparadise

class Terrain {
    lateinit var name: String
    var subtypes: TerrainSubtypes = TerrainSubtypes()
    var background: TerrainSubtypes? = null
    var physics: PhysicsTerrain = PhysicsTerrain()
    var vitals: VitalsTerrain = VitalsTerrain()
    var item: ItemTerrain = ItemTerrain()
}

class TerrainSubtypes {
    var sprite: String = ""
    var topLeft: TerrainSubtype = TerrainSubtype.DEFAULT
    var topRight: TerrainSubtype = TerrainSubtype.DEFAULT
    var bottomLeft: TerrainSubtype = TerrainSubtype.DEFAULT
    var bottomRight: TerrainSubtype = TerrainSubtype.DEFAULT

    var topCenter: TerrainSubtype = TerrainSubtype.DEFAULT
    var left: TerrainSubtype = TerrainSubtype.DEFAULT
    var bottomCenter: TerrainSubtype = TerrainSubtype.DEFAULT
    var right: TerrainSubtype = TerrainSubtype.DEFAULT

    var center: TerrainSubtype = TerrainSubtype.DEFAULT

    var verticalSolo: TerrainSubtype = TerrainSubtype.DEFAULT
    var horizontalSolo: TerrainSubtype = TerrainSubtype.DEFAULT
}

class TerrainSubtype {
    companion object {
        val DEFAULT = TerrainSubtype()
    }
    var image: String = ""
    var rotation: Int = 0
}

class ItemTerrain {
    var canPickUp: Boolean = true
    var maximumStackSize: Int = 99
    var sprite: String = "<UNSET>"
    var image: String = "<UNSET>"
    var weight: Float = 1F
}

fun ItemTerrain.asVisual() = object : GameAssets.AtlasSpriteVisual {
    override val imageFilename = sprite
    override val atlasKey = image
}

class VitalsTerrain {
    var health: Int = 5
    var hardness: Int = 0
    var vulnerabilities: List<String> = emptyList()
}

class PhysicsTerrain {
    var density: Float = 1F
}

class VisualTerrain {
    var topLeftImage: String? = null
    var topCenterImage: String? = null
    var topRightImage: String? = null
    var leftImage: String? = null
    var centerImage: String? = null
    var rightImage: String? = null
    var bottomLeftImage: String? = null
    var bottomCenterImage: String? = null
    var bottomRightImage: String? = null
    var defaultImage: String? = null
    var sprite: String? = null
}
