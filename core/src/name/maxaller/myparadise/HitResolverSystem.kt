package name.maxaller.myparadise

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.systems.IteratingSystem
import com.google.inject.Inject

class HitResolverSystem @Inject constructor(): IteratingSystem(Family.all(JustHitComponent::class.java).get()) {
    override fun processEntity(entity: Entity, deltaTime: Float) {
        val justHit = entity.get(JustHitComponent)
        entity.remove(JustHitComponent)

        if (entity.has(CreatureAiComponent.Bounce)) { // hack for detecting creatures
            val damage = justHit.magnitude.toInt()
            val damageResistance = 10
            val effectiveDamage = if (damage > damageResistance) {
                damage - damageResistance
            } else {
                0
            }

            if (effectiveDamage > 0) {
                val damageType = justHit.weapon.get(WeaponAssetComponent).weapon.damageType.name.toLowerCase()
                val hp = entity.get(HitpointsComponent)
                hp.current -= effectiveDamage
                val remainingHp = hp.current
                println("Weapon did $effectiveDamage $damageType damage; enemy now has $remainingHp hp")
            } else {
                println("The weapon just bounced off!")
            }
        } else if (entity.has(TerrainComponent)) { // hit the ground
            val damage = calculateDamage(justHit) * softTerrainMultiplier(justHit)
            val effectiveDamage = damage.toInt()
            if (effectiveDamage > 0 && entity.has(HitpointsComponent)) {
                val hp = entity.get(HitpointsComponent)
                hp.current -= damage.toInt()
                println("Weapon did $effectiveDamage damage to the terrain, leaving it with ${hp.current} hp")
            }
        } else if (entity.has(HitpointsComponent)) {
            println("hit thing with hitpoints")
            val damage = calculateDamage(justHit)
            val effectiveDamage = damage.toInt()
            if (effectiveDamage > 0) {
                val hp = entity.get(HitpointsComponent)
                hp.current -= damage.toInt()
                println("Weapon did $effectiveDamage damage, leaving it with ${hp.current} hp")
            }
        }
    }

    private fun calculateDamage(justHit: JustHitComponent) = justHit.magnitude

    private fun softTerrainMultiplier(justHit: JustHitComponent) = justHit.weapon.get(WeaponComponent).weaponDescription.baseWeapon.damageMultiplierSoftTerrain
}
