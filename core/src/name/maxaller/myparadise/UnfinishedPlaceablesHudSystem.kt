package name.maxaller.myparadise

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.EntityListener
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.ashley.utils.ImmutableArray
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.ui.Stack
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.Scaling
import com.google.inject.Inject
import java.util.*

class UnfinishedPlaceablesHudSystem @Inject constructor(private val hud: Hud, private val assets: MyAssetManager, private val entityCreator: EntityCreator, private val cam: OrthographicCamera) : IteratingSystem(Family.all(TransformComponent::class.java, UnfinishedPlaceableComponent::class.java).get()) {
    private val skin = assets.skin
    private val map = HashMap<Entity, Table>()
    private lateinit var players: ImmutableArray<Entity>

    override fun addedToEngine(engine: Engine) {
        super.addedToEngine(engine)
        engine.addEntityListener(family, object : EntityListener {
            override fun entityRemoved(entity: Entity) {
                map.remove(entity)?.remove()
            }

            override fun entityAdded(entity: Entity) {
            }
        })
        players = engine.getEntitiesFor(Family.all(PlayerComponent::class.java, InventoryComponent::class.java).get())
    }

    private inner class CraftButtonListener(private val entity: Entity, private val ingredients: Map<String, Int>) : ChangeListener() {
        override fun changed(event: ChangeEvent, actor: Actor) {
            println("Clicked!")
            val inventory = players.first().get(InventoryComponent)
            var hasAllIngredients = true

            val grassCount = inventory.count(Items.GrassItem)
            println("Player has $grassCount (${Items.GrassItem.name})")
            for ((ingredientName, quantity) in ingredients) {
                val hasItem = inventory.items.any {
                    if (it.has(ItemTypeComponent)) {
                        it.get(ItemTypeComponent).itemType.name == ingredientName
                    } else {
                        false
                    }
                }
                if (!hasItem) {
                    hasAllIngredients = false
                }
                println("$ingredientName -> $quantity")
            }
            if (hasAllIngredients) {
                var success = true
                for ((ingredientName, quantity) in ingredients) {
                    if (!inventory.take(Items.GrassItem, quantity)) {
                        success = false
                    }
                }
                if (success) {
                    entity.remove(UnfinishedPlaceableComponent::class.java)
                } else {
                    // UH-OH?!
                }
            } else {
                println("no soup for you -- come back with more grass")
            }
        }
    }

    override fun processEntity(entity: Entity, deltaTime: Float) {
        if (!map.containsKey(entity)) {
            // create table
            val table = Table(skin).apply {
                setBackground("default-pane")
                row().width(100F)
            }
            val ingredients = entity.get(UnfinishedPlaceableComponent).requiredIngredients
            for ((ingredientName, quantity) in ingredients) {
                val image = entityCreator.getImage(ingredientName)
                val verticalGroup = Table()
                verticalGroup.add(Image(SpriteDrawable(image), Scaling.fit)).height(16F).fillX().expandX()
                verticalGroup.row()
                verticalGroup.add(Label("0/$quantity", skin))
                verticalGroup.row()
                verticalGroup.add(Label(ingredientName, skin))
                table.add(verticalGroup)
            }
            table.row()
            table.add(TextButton("Craft", skin).apply {
                addListener(CraftButtonListener(entity, ingredients))
//                isDisabled = true
            })
            table.apply {
                pack()
                val coords = cam.unproject(Vector3(entity.transform.position))
                table.setPosition(coords.x, coords.y)
            }
            hud.stage.addActor(table)
            map[entity] = table
        } else {
            val table = map[entity]!!
            val coords = cam.project(Vector3(entity.transform.position))
            coords.x -= table.width / 2
            table.setPosition(coords.x, coords.y)
        }
    }

    private fun Stack.addAmountOverlay(amount: Int) {
        val overlay = Table(assets.skin)
        if (amount > 1) {
            overlay.add(Label(amount.toString(), assets.skin).apply {
                setAlignment(Align.right or Align.bottom)
            }).expand().bottom().right().padRight(5F)
            addActor(overlay)
        }
    }
}
