package name.maxaller.myparadise

import com.badlogic.ashley.core.Component
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.physics.box2d.Filter
import com.google.inject.Inject
import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.apache.commons.math3.util.Pair as MathPair

class HitPointsSystem @Inject constructor(private val blockBuilder: BlockBuilder,
                                          private val entityCreator: EntityCreator) : IteratingSystem(Family.all(HitpointsComponent::class.java).get()) {
    override fun processEntity(entity: Entity, deltaTime: Float) {
        val hp = entity.get(HitpointsComponent)
        if (hp.current <= 0) {
            engine.removeEntity(entity)
            handleDrops(entity)
        }
    }

    private fun handleDrops(entity: Entity) {
        if (entity.has(TerrainComponent)) {
            val detached = blockBuilder.createDetachedBlock(entity.get(TerrainComponent).instance.terrain)
            detached.apply {
                engine.addEntity(this)
                val filter = Filter().apply { categoryBits = PhysicsSystem.CATEGORY_SCENERY }
                physics.body.fixtureList.forEach { fixture ->
                    fixture.isSensor = false
                    fixture.filterData = filter
                }
                physics.body.setTransform(entity.physics.body.position, entity.physics.body.angle)
            }
        } else if (entity.has(ItemDropsComponent)) {
            val targetPosition = entity.physics.body.position
            entity.get(ItemDropsComponent).generateDrop().map { entityCreator.createItem(it) }.forEach { item ->
                item.physics.body.setPosition(targetPosition)
            }
        }
    }
}

class ItemDropsComponent(private val data: String) : Component {
    companion object : ComponentResolver<ItemDropsComponent>(ItemDropsComponent::class.java)

    fun generateDrop() = ItemDropsDataRepo[data].generateDrop()
}

interface ItemDropsData {
    fun generateDrop(): List<String>
}

object ItemDropsDataRepo {
    private val probabilities = mapOf(
            Pair("grass", ProbabalisticItemDropData(EnumeratedDistribution<String>(listOf(
                    Pair("grass_item", 10.0),
//                    Pair("grass_seeds", 1.0),
                    Pair(null as String?, 2.0)
            ).map { it.toMathPair() })))
    )

    private class ProbabalisticItemDropData(private val dist: EnumeratedDistribution<String>) : ItemDropsData {
        override fun generateDrop(): List<String> {
            val value = dist.sample()
            return if (value.isNullOrEmpty()) {
                emptyList()
            } else {
                listOf(value)
            }
        }
    }

    operator fun get(str: String): ItemDropsData = probabilities[str] ?: throw IllegalArgumentException("$str not found")
}

fun <K, V> Pair<K, V>.toMathPair() = MathPair(first, second)
