package name.maxaller.myparadise

import com.badlogic.ashley.core.Component
import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import java.util.*

class IntervalIteratingSystemBucketsTest {
    object Foo : Component

    @Test
    fun sanity() {
        val processedEntities = HashSet<Entity>()
        val system = object : IntervalIteratingSystemBuckets(Family.all(Foo::class.java).get(), 10F, 0, 5) {
            override fun processEntity(entity: Entity) {
                processedEntities.add(entity)
            }
        }
        val engine = Engine()
        engine.addSystem(system)
        for (i in 1..100) {
            engine.addEntity(Entity().apply { add(Foo) })
        }
        system.update(1F)
        assertThat(processedEntities).isEmpty()
        system.update(2F)
        assertThat(processedEntities.size).isBetween(10, 30)
        system.update(2F)
        assertThat(processedEntities.size).isBetween(30, 50)
        system.update(2F)
        assertThat(processedEntities.size).isBetween(50, 70)
        system.update(2F)
        assertThat(processedEntities.size).isBetween(70, 90)
        system.update(2F)
        assertThat(processedEntities).hasSize(100)
        system.update(2F)
        assertThat(processedEntities).hasSize(100)
    }
}